<?php
/*
Template Name: Archives
*/
get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-9 dmbs-main">
    
    	<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
   		 	<?php if(function_exists('bcn_display'))
    		{
       			bcn_display();
		    }?>
		</div>
		
		<h2>Archives by Month:</h2>
        <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
            <option value=""><?php esc_attr( _e( 'Select Month', 'textdomain' ) ); ?></option> 
            <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
        </select>
		<ul>
			<?php wp_get_archives('type=monthly&limit=12'); ?>
		</ul>

   </div>

   <?php //get the right sidebar ?>
   <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>