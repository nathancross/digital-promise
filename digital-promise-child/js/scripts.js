jQuery(document).ready(function($) {
  

    

    
    
    
    
    
    
    // SEARCH BAR
	// Get a reference to the container.
	var container = $( ".search-bar" );
	var searchButton = $( ".search-icon" );
	
	//slide down as soon as page loads. note: hiding it or setting interval to zero breaks scroll bar
	container.slideUp( 1 );

	// Bind the link to toggle the slide.
	$( ".search-icon" ).click(
		function( event ){
			// Prevent the default event.
			event.preventDefault();

			// Toggle the slide based on its current visibility.
			if (container.is( ":visible" )){

				// Hide - slide up.
				container.slideUp( 200 );

			} else {

				// Show - slide down.
				container.slideDown( 500 );
			}
		}
	);
    
    $(".search-bar #s").attr("value", "Start typing...");
    $(".search-bar #s").focus(function() {
        if (!$(this).data("Start typing...")) $(this).data("Start typing...", $(this).val());
        if ($(this).val() != "" && $(this).val() == $(this).data("Start typing...")) $(this).val("");
    }).blur(function(){
        if ($(this).val() == "") $(this).val($(this).data("Start typing..."));
    });

    
    
    /* NEWSLETTER SIGN UP BUTTON */
    $( ".sign-up-button" ).click(
		function( event ){
            //alert('some sort of signup thing goes here...');
		}
	);
    
    
    
    
    /* WAYPOINTS
    var waypoint = new Waypoint({
      element: document.getElementById('basic-waypoint'),
      handler: function() {
        console.log('Basic waypoint triggered')
      }
    }) 
    */
    
    
    
    
    
    /**
     * cbpAnimatedHeader.js v1.0.0
     * http://www.codrops.com
     *
     * Licensed under the MIT license.
     * http://www.opensource.org/licenses/mit-license.php
     * 
     * Copyright 2013, Codrops
     * http://www.codrops.com
     */
    var cbpAnimatedHeader = (function() {

        var docElem = document.documentElement,
            header = document.querySelector( '.cbp-af-header' ),
            header2 = document.querySelector( '.wprmenu_bar' ),
            didScroll = false,
            changeHeaderOn = 300;

        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }

        function scrollPage() {
            var sy = scrollY();
            if ( sy >= changeHeaderOn ) {
                classie.add( header, 'cbp-af-header-shrink' );
                classie.add( header2, 'cbp-af-header-shrink' );
            }
            else {
                classie.remove( header, 'cbp-af-header-shrink' );
                classie.remove( header2, 'cbp-af-header-shrink' );
            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        init();

    })();
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
/* jquery end */    
})