<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


<div class="row dmbs-content">
        <div class="homepage-feature">
        <?php 

        $args = array( 'post_type' => 'homepage-hero', 'posts_per_page' => 1 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

                    <div <?php post_class(); ?>>

                        <?php get_template_part( 'content', 'page' ); ?>

                            <?php if ( has_post_thumbnail() ) : ?>
                                               <?php the_post_thumbnail('full'); ?>
                                <div class="clear"></div>
                            <?php endif; ?>
                        <div class="feature-overlay">
                            <h1><?php the_title() ;?></h1>
                            <a class="btn btn-gold btn-action" href="<?php the_field('learn_more_link'); ?>"><?php the_field('learn_more_button_text'); ?></a>
                        </div>
                        
                        </div>

        
		<?php endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div>
        
</div>
<!-- / end homepage hero/feature area -->








<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- begin homepage story grid -->
<div class="row dmbs-content" id="basic-waypoint"> 
    <div class="col-md-12">
            
<?php

$args = array(
             'post_type' => array('post', 'tool'), 
             'posts_per_page' => 5000, 
             'orderby' => 'meta_value', 
             'order' => 'ASC',
             'post_status' => 'publish',
             'nopaging' => true,
             'meta_query' => array(
                 array(
                    'key' => 'front_order',
                    'value' => array(1, 2, 3, 4, 5, 6),
                    'compare' => 'IN'
                 )
             ),
        );

$loop = new WP_Query( $args );
$i = 0;

while ( $loop->have_posts() ) : $loop->the_post();
$i++;

    if($i == 1)
    {
        echo '<div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">';
    }
    elseif($i == 4)
    {
        echo '</div><div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">'; 
    }
    elseif($i == 7)
    {
        echo '</div>';
        break;
    }

    if($i == 1 || $i == 6)
    {
        echo '
        <div class="Grid-cell u-large-1of2 u-med-full u-small-full dp0' . $i . ' ' . get_post_type() . '">

            <div class="callout">
                <a href="' . get_permalink(get_the_ID()) . '" class="callout-link">';
                if(get_the_post_thumbnail()) {
                    $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size = 'large', $icon = false );
                    echo '<img src="' . $image_attributes[0] . '">';
                }

        echo                    '<h3>' . get_the_title() . '</h3>
                            <p>' . get_the_excerpt() . '</p>
                </a>
            </div>

        </div>';
    }
    else
    {
        echo '
        <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full dp0' . $i . ' ' . get_post_type() . '">

            <div class="callout">
                <a href="' . get_permalink(get_the_ID()) . '" class="callout-link">';
                if(get_the_post_thumbnail()) {
                    $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size = 'large', $icon = false );
                    echo '<img src="' . $image_attributes[0] . '">';
                }

                echo            '<h3>' . get_the_title() . '</h3>
                            <p>' . get_the_excerpt() . '</p>
                </a>
            </div>

        </div>';
    }

endwhile; // end of the loop. 

wp_reset_postdata(); 

?>
            
    </div><!-- /end col-12 -->
</div><!-- /end row dmbs-content -->



        
<div class="row dmbs-content">
    <div class="col-md-12">
        <div class="mission-statement"><h2>Digital Promise works at the intersection of education leaders, researchers, and learning technology developers to improve learning opportunity.</h2>
            <h2>We are guided by the following principles:</h2></div>
    </div>
</div>

<div class="row dmbs-content">
    <div class="col-md-12">
            <div class="Grid">
                               
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                <div class="callout-round">
                    <a href="/our-approach/"><img src="/wp-content/uploads/sites/4/2016/03/dp-circle-network@2x.png"></a>
                            <h3><a href="/our-approach/#networks">Networks</a></h3>
                            <p>to connect us with people and ideas</p>                  
                </div>
            </div>                     
                               
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                <div class="callout-round">
                    <a href="/our-approach/"><img src="/wp-content/uploads/2016/03/dp-circle-stories-characters@2x.png"></a>
                            <h3><a href="/our-approach/#stories">Stories</a></h3>
                            <p>to inspire ideas and incent action</p>                  
                </div>
            </div>
            
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                <div class="callout-round">
                    <a href="/our-approach/"><img src="/wp-content/uploads/sites/4/2016/03/dp-circle-research@2x.png"></a>
                            <h3><a href="/our-approach/#research">Research</a></h3>
                            <p>to inform, ground, and support decision-making</p>                  
                </div>
            </div>
                
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                <div class="callout-round">
                    <a href="/our-approach/"><img src="/wp-content/uploads/sites/4/2016/03/dp-circle-engagement@2x.png"></a>
                            <h3><a href="/our-approach/#engagement">Engagement</a></h3>
                            <p>to motivate learning for life</p>                  
                </div>
            </div>
                
        </div>
    </div>
</div>
<div class="row dmbs-content">
    <div class="col-md-12" style="text-align:center; padding:20px 0">
        <a style="float:none;" href="/our-approach/" class="btn btn-blue-light btn-action">Learn more about our approach</a>
    </div>
</div>










<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<div class="row dmbs-content">
    <div class="col-md-12">

        

        
        <!----------------------------------------------------->

   </div>

</div>




</div>
<!-- end content container -->

<?php get_footer(); ?>
