<?php /* Template Name: Landing page */ ?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">
        <div class="homepage-feature">
                <?php if ( has_post_thumbnail() ) : ?>
                   <?php the_post_thumbnail("full"); ?>
                    <div class="clear"></div>
                    <div class="feature-overlay">
                        <h1><?php the_title() ;?></h1>
                    </div>
                <?php endif; ?>
        </div>

    <div class="col-md-12 dmbs-main">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php wp_link_pages(); ?>
            <?php comments_template(); ?>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
