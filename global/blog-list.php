<?php
/**
 * Template Name: Blog List
 */
get_header(); 
get_template_part('template-part', 'head'); 
get_template_part('template-part', 'topnav'); 
?>

<div class="row dmbs-content">

	<div class="col-md-12 dmbs-main">    
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/" style="display:none;">
    	<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
		</div>
    <h2 class='page-header'>Blog</h2>
  </div>

  <div class="col-md-12">
    <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
      <?php 
      $args = array( 'post_type' => 'post', );
      $loop = new WP_Query( $args );
      if ( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">
	        <div class="callout">
	          <div <?php post_class(); ?>>                 
	            <?php 
	            echo '<a href="' . get_permalink(get_the_ID()) . '" class="callout-link">';
	            if (get_the_post_thumbnail()) {
	              $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size = 'large', $icon = false );
	              echo '<img src="' . $image_attributes[0] . '">';
	            }
	            echo '<p class="byline vcard" style="margin-bottom:1em;"><time class="updated" datetime="' . get_the_date('Y-m-d') . '" itemprop="datePublished">' . get_the_date() . '</time></p><h3>' . get_the_title() . '</h3><p>' . get_the_excerpt() . '</p></a>';
	            ?>
						</div>
	        </div>
				</div>
				<?php endwhile; ?>
		</div><!-- /end grid container -->

		  <?php 
		  	if(function_exists('wp_paginate')) {
		      wp_paginate();
		    } else { posts_nav_link(); }
		    wp_reset_postdata();
	    else: get_404_template(); 
	    endif; 
	    ?>
	</div>
	
</div>

<?php get_footer(); ?>