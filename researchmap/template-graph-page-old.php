<?php
/*
Template Name: Data Display Graph
*/
?>
<?php get_header(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/mapFunctions.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/dataSidebar.js"></script>
	
	<div id="content">
	
		<div id="inner-content" class="row">
        
        <?php get_sidebar("sidebarFilter"); ?>
	
		    <main id="main" class="large-12 medium-8 columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->

		<?php get_sidebar("sidebarData"); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>