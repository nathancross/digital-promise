<?php
/*
Template Name: Topic Landing page
*/
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header waypoint"><?php the_title() ;?></h2>

        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
        <?php
        
        $args = array(
        	'post_type'      => 'page',
    		'posts_per_page' => -1,
   			'post_parent'    => $post->ID,
    		'order'          => 'ASC',
    		'orderby'        => 'menu_order'
        );
        
        
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                       
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                           
                           <?php if ( has_post_thumbnail() ) : ?>
                               <?php the_post_thumbnail("large"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>

                            <h3>
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h3>

                            
                            <?php the_excerpt(); ?>

                       </div>
                    </div>

            </div>

		<?php endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div><!-- /end grid container -->


        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
