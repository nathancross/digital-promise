<?php // Opening PHP tag - nothing should be before this, not even whitespace
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Search Bar',
		'id'            => 'search_bar_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
    
	register_sidebar( array(
		'name'          => 'Utility Nav Bar',
		'id'            => 'utility_nav_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );    
    
	register_sidebar( array(
		'name'          => 'Visit Left',
		'id'            => 'visit_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );  
    
	register_sidebar( array(
		'name'          => 'Visit Right',
		'id'            => 'visit_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );  
    
	register_sidebar( array(
		'name'          => 'Footer Left',
		'id'            => 'footer_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );  
    
	register_sidebar( array(
		'name'          => 'Footer Right',
		'id'            => 'footer_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
    register_sidebar(array(
		'id' => 'sidebarFilter',
		'name' => __('Sidebar Map Filters'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	
    register_sidebar(array(
		'id' => 'sidebarData',
		'name' => __('Sidebar Data Results'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
}
add_action( 'widgets_init', 'arphabet_widgets_init' );





// LOAD SCRIPTS
add_action( 'wp_enqueue_scripts', 'add_my_script' );
function add_my_script() {
    wp_enqueue_script(
        'my-scripts', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );    
    wp_enqueue_script(
        'my-scripts1', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/classie.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );        
    wp_enqueue_script(
        'my-scripts2', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/scripts.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );
    wp_enqueue_script(
        'my-scripts3', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/mapFunctions.js'
    );
    wp_enqueue_script(
        'my-scripts4', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/dataSidebar.js'
    );
	
	wp_enqueue_script(
        'my-scripts5', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/jQuery.print.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    ); 
}





// Changing excerpt length
function new_excerpt_length($length) {
return 16;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Changing excerpt more
function new_excerpt_more($more) {
return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');










// Add Custom Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
    	'utility_navigation' => __( 'Utility Navigation' )
    )
  );
} 
add_action( 'init', 'register_my_menus' );

function default_utility_nav() { // HTML markup for a default message in menu location
	echo "<ul class='nav'>					
		<li>Create the Utility Navigation</li>
	</ul>";
}

function get_user_tiles(){
	global $current_user;
    if (is_user_logged_in()){ //check if user is logged in.
        // get current user info
        get_currentuserinfo();
        $old_notes = get_user_meta($current_user->ID, 'user_tiles', true);
		
		if (!isset($old_notes[0])) {
			$old_notes = '{"My Group 1":[]}';
		}
        echo($old_notes);
	
    }
	else{
		echo("LOGGEDOUT");
	}
	die();
}



function save_user_tiles(){
	 global $current_user;
	 
	 $tiles = $_POST['Tiles'];
    if (is_user_logged_in()){ //check if user is logged in.
        if (isset($_POST['Tiles'])){
            // get current user info
            get_currentuserinfo();
            $old_notes = get_user_meta($current_user->ID, 'user_tiles', true);
			echo("updating myTiles for user ".$current_user->ID);
			//first note we are saving fr this user
			update_user_meta( $current_user->ID, 'user_tiles', $_POST['Tiles']);
            
        }
		echo("Tiles saved");
    } 
	else{
		echo("Not logged in, cannot save");
	}
	
	die();
}

function clear_user_tiles(){
	 global $current_user;
	 
	 if (is_user_logged_in()){
		  get_currentuserinfo();
		  update_user_meta( $current_user->ID, 'user_tiles', null);
		  echo("user tiles cleared");
	 }
	 else{
		 echo ("no user logged in");
	 }
	 die();
}

add_action('wp_ajax_get_user_tiles', 'get_user_tiles');
add_action('wp_ajax_nopriv_get_user_tiles', 'get_user_tiles');

add_action('wp_ajax_save_user_tiles', 'save_user_tiles');
add_action('wp_ajax_nopriv_save_user_tiles', 'save_user_tiles');

add_action('wp_ajax_clear_user_tiles', 'clear_user_tiles');
add_action('wp_ajax_nopriv_clear_user_tiles', 'clear_user_tiles');

//