<?php
/*
Template Name: Topic Detail page
*/
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">


	<?php if ( has_post_thumbnail() ) : ?>
	   <?php the_post_thumbnail("full"); ?>
			<div class="homepage-feature">
				<div class="clear"></div>
				<div class="feature-overlay">
					<h1><?php the_title() ;?></h1>
				</div>
			</div>
	<?php endif; ?>


    <div class="col-md-9 dmbs-main">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <?php if ( !has_post_thumbnail() ) : ?>
				<h2 class="page-header waypoint"><?php the_title() ;?></h2>
        	<?php endif; ?>
            <?php the_content(); ?>
            <?php wp_link_pages(); ?>
            <?php comments_template(); ?>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>
    
    	<?php //right sidebar ?>
	    <?php get_sidebar( 'right' ); ?>
    

</div>
<!-- end content container -->

<?php get_footer(); ?>
