<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/corpusStyles.css">
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/wholeDESCR.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/corpusScript.js"></script>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main corpus">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header waypoint"><?php the_title() ;?></h2>
            <p>Lorem ipsum dolor sit amet, eam et novum zril quidam. In quod mediocrem comprehensam sed, duo ex alia insolens, vel habemus gloriatur ut. At per alia autem officiis. Solet postea et mel. Mea ei consul alienum sensibus.</p>
            <div class="col-md-4">
            <h2> Topics</h2>
            <p>Ei erant animal molestiae pro, nec cu semper utamur. Tation prompta cu eos, id augue iisque vim.</p>
            <select id="topicSelect"></select>
            <select id="subtopicSelect"></select>
            <h2>Field</h2>
            <select id="fieldSelect">
            	<option id="MCPselect" value="MCP" disabled>Most Cited Papers</option>
                <option id="MRPselect" value="MRP" disabled >Highly Representative Papers</option>
                <option id="MCAselect" value="MCA" disabled>Most Cited Authors</option>
                <option value="R">Most Frequent References</option>
                <option value="K" selected>Most Frequent Keywords</option>
                <option value="T">Most Frequent Title words</option>
                <option value="I">Most Frequent Institutions</option>
                <option value="J">Most Frequent Journals Sources</option>
               <option value="S">Most Frequent Subject Categories</option>
                <option value="Y">Most Frequent Publication Years</option>' 
            </select>
            </div>
            <div id="corpusTableHolder" class="col-md-8">
                <table id='corpusTable'>
                	<colgroup>
                    	<col span="1" style="width:15%">
                        <col span="1" style="width:55%">
                        <col span="1" style="width:15%">
                        <col id="fourthColumn" span="1" style="width:15%">
                    </colgroup>
                    <tr>
                    	<th class="first">RANK</th>
                        <th id="fieldHeader" class="second">FIELD</th>
                        <th class="third"></th>
                        <th class="fourth"></th>
                    </tr>
                    <tr class="nonHeader">
                    	<td class="first" colspan="2">LOADING</td>
                    </tr>
                </table>
            </div>
            

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
