<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">

        <?php

            //if this was a search we display a page header with the results count. If there were no results we display the search form.
            if (is_search()) :

                 $total_results = $wp_query->found_posts;

                 echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

                 if ($total_results == 0) :
                     get_search_form(true);
                 endif;

            else:
                echo "<h2 class='page-header'>Search</h2>";

            endif;

        ?>


    </div>
</div>

<div class="row dmbs-content">
    <div class="col-md-12">
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post (unused in this template)
                    if ( is_single() ) : ?>
                        
                    <?php
                    // list of posts
                    else : ?>
                    
            <?php
            //echo "is_featured = ";
			$is_featured = get_post_meta($post->ID, 'is_featured', TRUE);
			//if(!$is_featured) {
			?>
			
			<div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                           
                           <?php if ( has_post_thumbnail() ) : ?>
                               <?php the_post_thumbnail("medium"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>
							
							<p class="byline vcard" style="margin-bottom:1em;">
								<time class="updated" datetime="<?php echo get_the_date('Y-m-d'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
							</p>

                            <h3>
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h3>

                            
                            <?php the_excerpt(); ?>

                       </div>
                    </div>

            </div>

			<?php
			//}
			?>

				
                     <?php  endif; ?>

                <?php endwhile; ?>
            
        </div><!-- /end grid container -->
                <?php if(function_exists('wp_paginate')) {
                    wp_paginate();
                    }
                    else {
                        posts_nav_link();
                    }
                ?> 
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>
    </div>
   </div>
<!-- end content container -->

<?php get_footer(); ?>

