<?php /* Template Name: Our Work Landing Page */ ?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">
    
        
                <?php if ( has_post_thumbnail() ) : ?>
                   <?php the_post_thumbnail("full"); ?>
                    <div class="homepage-feature">
                        <div class="clear"></div>
                        <div class="feature-overlay">
                            <h1><?php the_title() ;?></h1>
                        </div>
                    </div>
                <?php endif; ?>

    
    <div class="col-md-12 dmbs-main">
        
        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
            <?php the_content(); ?>

        <?php endwhile; ?>
        <?php endif; ?>

		<!-- determine the current sector to display -->
		<?php  $this_cat = get_query_var( 'cat', 0 );  ?>

        <!-- form style="margin:0 auto 20px auto; width:500px;" id="category-select" class="category-select" action="<?php echo $_SERVER["REQUEST_URI"]; ?>#category-select" method="get" -->

            <?php
            $which_selected = 0;
            if ($this_cat > 0) {
            	$which_selected = $this_cat;
            }
            
            $args = array(
                'show_option_all' => __( 'All sectors' ),
                'echo'             => 0,
                'child_of'		   => 25,
                'selected'		   => $which_selected,
            );
            ?>

            <?php //$select  = wp_dropdown_categories( $args ); ?>
            <?php //$replace = "<label for='cat'>View initiatives by sector:</label> <select$1 onchange='return this.form.submit()'>"; ?>
            <?php //$select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

            <?php //echo $select; ?>

            <!-- noscript>
                <input type="submit" value="View" />
            </noscript>

        </form -->
        
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
        <?php 

        // assign category; if queried, use Initiatives plus query
        $which_cats = array();
        array_push($which_cats, 31);
        if ($this_cat > 0) {
        	array_push($which_cats, $this_cat);
        }
        
        $args = array(
        	'post_type' => 'initiative', 
        	'posts_per_page' => 500, 
        	'orderby' => 'title', 
        	'order' => 'ASC',
        	'post_status' => 'publish',
   	        'category__and' => $which_cats,
        );
        
        
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
        
        	<?php 
        	$thisLoopCount = $loop->found_posts;
        	
        	//echo $thisLoopCount;
        	
        	if ($thisLoopCount >= 3) { 
        	
        		//echo "=3?"; ?>
                       
            	<div class="Grid-cell u-large-1of3 u-med-full u-small-full">
            
            <?php } else if ($thisLoopCount == 2) { 
        	
        		//echo "=2?"; ?>
                       
            	<div class="Grid-cell u-large-2of4 u-med-1of2 u-small-full double-initiative">

            <?php } else if ($thisLoopCount == 1) { 
        	
        		//echo "=1?"; ?>
                       
            	<div class="Grid-cell u-large-full u-med-1of2 u-small-full single-initiative">
            
            <?php } else { ?>
                       
            	<div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

            <?php } ?>

                <div class="callout">
                    <div <?php post_class(); ?>>
                           
                           <?php 
                            echo '
                                    <a href="' . get_permalink(get_the_ID()) . '" class="callout-link">';
                                    
                                    if(get_the_post_thumbnail()) {
                                        $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size = 'large', $icon = false );
                                        echo '<img src="' . $image_attributes[0] . '">';
                                    }

                                    echo            '<h3>' . get_the_title() . '</h3>
                                                <p>' . get_the_excerpt() . '</p>
                                    </a>
                                ';
                        ?>

                       </div>
                    </div>

            </div>

		<?php endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div><!-- /end grid container -->

            

   </div>


</div>
<!-- end content container -->

<?php get_footer(); ?>

