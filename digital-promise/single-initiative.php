<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">
    
        
                <?php if ( has_post_thumbnail() ) : ?>
                   <?php the_post_thumbnail("full"); ?>
                        <div class="homepage-feature">
                            <div class="clear"></div>
                            <div class="feature-overlay">
                                <h1><?php the_title() ;?></h1>
                            </div>
                        </div>
                <?php endif; ?>
        
    
    <div class="col-md-12 breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
        <?php if(function_exists('bcn_display'))
        {
            bcn_display();
        }?>
    </div>

    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>

    <div class="col-md-7 dmbs-main">
        

        <?php if ( !has_post_thumbnail() ) : ?>
           <h2 class="page-header" style="margin-top:0;"><?php the_title() ;?></h2>
        <?php endif; ?>
        
        
        <?php

            //if this was a search we display a page header with the results count. If there were no results we display the search form.
            if (is_search()) :

                 $total_results = $wp_query->found_posts;

                 echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

                 if ($total_results == 0) :
                     get_search_form(true);
                 endif;

            endif;

        ?>

            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>
                            <!-- h2 class="page-header"><?php the_title() ;?></h2 -->
                            <?php the_content(); ?>
                            <!-- categories -->
                            <div class="category-list"><?php printf( __( 'Category', 'devdmbootstrap3' ).': %1$s', get_the_category_list(', ') ); ?></div>
                            <?php wp_link_pages(); ?>

                        </div>
                    

                     <?php  endif; ?>

                <?php endwhile; ?>
                <?php posts_nav_link(); ?>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>

   </div>

   <?php //get the right sidebar ?>
   <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

