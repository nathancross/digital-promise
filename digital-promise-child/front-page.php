<?php
/*
Template Name: Hero Homepage
*/
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


<div class="row dmbs-content">
        <div class="homepage-feature">
            <div class="subsite-name">Verizon Innovative Learning Schools</div>
        <?php 

        $args = array( 'post_type' => 'homepage-hero', 'posts_per_page' => 1 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

                    <div <?php post_class(); ?>>

                        <?php get_template_part( 'content', 'page' ); ?>

                            <?php if ( has_post_thumbnail() ) : ?>
                                               <?php the_post_thumbnail("full"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>
                        
                        </div>
        
		<?php endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div>
</div>
<!-- / end homepage hero/feature area -->


<!--   CONTENT AFTER HERO HERE -->



<div class="row dmbs-content">
   
   	<div class="col-md-12">
        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
            <?php get_404_template(); ?>
        <?php endif; ?>
   	</div>
   
</div>
<!-- end content container -->

<?php get_footer(); ?>
