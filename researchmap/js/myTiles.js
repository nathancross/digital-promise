// JavaScript Document


var _tiles;

//example get 
//jQuery.post("/wp-admin/admin-ajax.php",{action:"get_user_tiles"},function(results){});

//example set 
//jQuery.post("/wp-admin/admin-ajax.php",{action:"save_user_tiles",Tiles:jsonTiles},function(results){});

//example clear 
//jQuery.post("/wp-admin/admin-ajax.php",{action:"clear_user_tiles"},function(results){});

jQuery(function(){
	//jQuery.post("/wp-admin/admin-ajax.php",{action:"clear_user_tiles"},function(results){});
	jQuery("#tileArea").html("Loading Your Tiles");
	
	ingestSavedTiles();
	
	
});




function ingestSavedTiles(){
	jQuery.post("/wp-admin/admin-ajax.php",{action:"get_user_tiles"},
		function(results){
			console.log(results);
			if (results != "LOGGEDOUT")
				_tiles = JSON.parse(results);
			else
				_tiles = results;
				
			jQuery("#addGroupButton").click(addGroup);
			jQuery("#printSelectedButton").click(printSelected);
			buildDisplay();
		
		});
}



function buildDisplay(){
	
	if (_tiles == "LOGGEDOUT"){
		jQuery("#tileArea").html("<h2>You must be logged in to use MyTiles</h2>");
	}
	else{
		jQuery("#tileArea").html("");
		for (var key in _tiles){
			var group = jQuery("<div>").attr("class","tileGroup");
			
			var groupTopBar = jQuery("<div>").attr("class","groupTopBar f-grid");
			
			var groupTitle = jQuery("<h2>").attr("class","groupTitle groupTopLeft grid-i").html(key);
			var groupTitleInput = jQuery("<input>").attr('class',"groupTitleEdit groupTopLeft grid-i").val(groupTitle.html()).hide();
			
			var groupButtons = jQuery("<div>").attr("class","groupButtons f-grid grid-i");
			var editGroup = jQuery("<div>").attr("class","editGroupButton groupTopButton grid-i");
			var deleteGroup = jQuery("<div>").attr("class","deleteGroupButton groupTopButton grid-i");
			
			var tileHolder = jQuery("<div>").attr("class","tileHolder f-grid");
			group.append(groupTopBar);
			groupTopBar.append(groupTitle);
			groupTopBar.append(groupTitleInput);
			groupTopBar.append(groupButtons);
			
			groupButtons.append(editGroup);
			groupButtons.append(deleteGroup);
			group.append(jQuery("<div>").css({clear:'both'}));
			group.append(tileHolder);
			var tiles = _tiles[key];
			for (var i = 0; i < tiles.length;i++){
				var tile = makeTile(tiles[i],key);
				tileHolder.append(tile);
			}
			
			jQuery("#tileArea").append(group);
			
			setGroupListeners(group,key);
			
			
			
			
		}
		
		/*var maxim = 0;
		jQuery(".myTile").each(function(index, element) {
			console.log(jQuery(element).outerHeight());
            if (jQuery(element).outerHeight() > maxim)
				maxim = jQuery(element).outerHeight();
        });
		
		setTimeout(function(){jQuery(".myTile").css({height:maxim});},300);*/
	}
}

var _draggingData = {};
var _draggedFrom;

function setGroupListeners(group,key){
	
	var title = jQuery(".groupTitle",group);
	var editor = jQuery(".groupTitleEdit",group);
	var editBut = jQuery(".editGroupButton",group);
	var deleteBut = jQuery(".deleteGroupButton",group);
	
	editBut.click(function(){
		editGroupTitle(title,editor);
	});
	
	deleteBut.click(function(){
		if (window.confirm("Do you want to delete this entire group?"))
		deleteWholeGroup(title);
	});
	
	
	
	group.on("drop",dropTile);
	group.on("dragover",dragOverGroup);
	group.on("dragleave",dragOffGroup);
	
	function dropTile(evt){
		evt.preventDefault();  
		evt.stopPropagation();
		console.log("dropped");
		console.log(evt.originalEvent);
		
		var targ = jQuery(evt.currentTarget);
		targ.css({"background-color":"white"});
		
		var node = _draggingData;
		
		if (_tiles[key].indexOf(_draggingData)>= 0){
			
			console.log("same group");
		}
		else{
			_tiles[key].push(node);
			_tiles[_draggedFrom].splice(_tiles[_draggedFrom].indexOf(node),1);
			console.log("different group");
			tilesChanged();
		}
		
	}
	
	function dragOverGroup(evt){
		evt.preventDefault();  
		evt.stopPropagation();
		
		
		var targ = jQuery(evt.currentTarget);
		targ.css({"background-color":"#DDDDFF"});
		//console.log("dragged over");
		//console.log(evt);
	}
	
	function dragOffGroup(evt){
		evt.preventDefault();  
		evt.stopPropagation();
		
		var targ = jQuery(evt.currentTarget);
		targ.css({"background-color":"#FFFFFF"});
	}
}

function editGroupTitle(title,editor){
	
	var oldGroup = title.html();
	
	console.log("trying to edit the group");
	title.hide();
	editor.show();
	
	editor.on('keypress',function(event){
		if(event.which == 13) {
			editor.off('keypress');
			title.html(editor.val());
			title.show();
			editor.hide();
			
			var newGroup = title.html();
			
			_tiles[newGroup] = _tiles[oldGroup];
			
			delete _tiles[oldGroup];
			tilesChanged();
		}
	})
	
}

function deleteWholeGroup(title){
	delete _tiles[title.html()];
	tilesChanged();
	
}

function makeTile(node,inGroup){
	
	console.log(node);
	
	//var hold =jQuery("<div>").attr("class","grid-i");
	
	var nTile = jQuery("<div>").attr("class","myTile grid-i").attr("draggable","true");
	
	
	var check = jQuery("<input>").attr("type","checkbox").addClass("tileCheck dontPrint");
	var deleteBut = jQuery("<div>").attr("class","deleteTile dontPrint").html("X");
	
	nTile.append(check);
	nTile.append(deleteBut);
	nTile.append(jQuery("<div>").css({clear:"both"}));
	nTile.append("<strong><a class='dontPrint' href='"+wwwlink(node[0])+"' target='new'>"+node[0]+"</a><span class='printOnly'>"+node[0]+"</span></strong><br>");
	nTile.append(jQuery("<div>").addClass("tileInfoBlock").html(node[1]+"<br>"+node[2]+"("+node[3]+")<br><br>Citations:"+node[4]));
	nTile.append("Comments:<br>");
	var comArea = jQuery("<div>").attr("class","commentsBox comnothid");
	var comEditor = jQuery("<textarea>").attr("class","commentsEditor comhid").attr("maxlength",200).hide();
	var comSave = jQuery("<div>").attr("class","saveComments comhid").html("SAVE").hide();
	if (node[7]){
		comArea.append(node[7]);
		comEditor.val(node[7]);
	}
	nTile.append(comArea);
	nTile.append(comEditor);
	nTile.append(comSave);
	//hold.append(nTile);
	
	addTileListeners(node,nTile,inGroup);
	
	//jQuery("#saverNodeName").html(art[0]);
	//jQuery("#saverNodeInfo").html(art[1]+" in "+art[2]+"("+art[3]+")<br>Cited:"+art[4]);
	
	jQuery(".printOnly",nTile).hide();
	return nTile;
	
	
}

function wwwlink(foo) {
    foo="https://scholar.google.com/scholar?hl=com&q="+foo.replace(' ','+');
    return foo
  }

function addTileListeners(node,tile,inGroup){
	var deleteBut = jQuery(".deleteTile",tile);
	
	deleteBut.click(function(){
		
		var shouldDelete = window.confirm("Are you sure you want to remove this tile?");
		if (shouldDelete == true){
			console.log(node);
			console.log(tile);
			tile.remove();
			for (var group in _tiles){
				tiles = _tiles[group];
				if (tiles.indexOf(node) >= 0){
					_tiles[group].splice(_tiles[group].indexOf(node),1);
				}
			}
			
			tilesChanged();
		}
	});
	
	tile.on("dragstart",drag);
	
	function drag(evt){
		
		_draggedFrom = inGroup;
		_draggingData = node;
	
		console.log("item has been dragged");
		console.log(evt);
	}
	
	var coms = jQuery(".commentsBox",tile);
	coms.click(function(){
		jQuery(".comhid",tile).show();
		jQuery(".comnothid",tile).hide();
	})
	
	var save = jQuery(".saveComments",tile);
	save.click(function(){
		jQuery(".comhid",tile).hide();
		jQuery(".comnothid",tile).show();
		for (var group in _tiles){
				tiles = _tiles[group];
				if (tiles.indexOf(node) >= 0){
					_tiles[group][tiles.indexOf(node)][7] = jQuery(".commentsEditor",tile).val();
				}
			}
			
		coms.html(jQuery(".commentsEditor",tile).val());	
		tilesChanged();
	});
}

function addGroup(){
	var count = 0;
	
	var grouplist = [];
	
	for (var group in _tiles){
		grouplist.push(group);
		count++;
	}
	
	var title = "My Group "+(++count);
	while (grouplist.indexOf(title) > 0){
		title = "My Group "+(++count);
	}
	_tiles[title] = [];
	tilesChanged();

}

function printSelected(){
	
	var theTiles = jQuery(".myTile");
	
	var checkedTiles = theTiles.filter(function(index,element){
		var elem = jQuery(element);
		var check = jQuery(".tileCheck",elem);
		return check.is(":checked");
	})
	
	if (checkedTiles.length == 0)
		alert("no tiles selected");
	else{
		var printMe = jQuery("<div>");
		
		checkedTiles.each(function(index, element) {
            
			var ti = jQuery(element).clone();
			ti.css({width:"49%"});
			printMe.append(jQuery(element).clone());
			
			
        });
		console.log(printMe.get(0));
		
		jQuery(".dontPrint",printMe).hide();
		jQuery(".printOnly",printMe).show();
		
		printMe.print() ;
	}
}

function tilesChanged(){
	
	var jsonTiles = JSON.stringify(_tiles);
	jQuery("#tileArea").html("Loading Your Tiles");
	buildDisplay();
	
	
	jQuery.post("/wp-admin/admin-ajax.php",{action:"save_user_tiles",Tiles:jsonTiles},function(results){});
}