<?php // Opening PHP tag - nothing should be before this, not even whitespace
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Search Bar',
		'id'            => 'search_bar_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
    
	register_sidebar( array(
		'name'          => 'Utility Nav Bar',
		'id'            => 'utility_nav_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );    
    
	register_sidebar( array(
		'name'          => 'Visit Left',
		'id'            => 'visit_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );  
    
	register_sidebar( array(
		'name'          => 'Visit Right',
		'id'            => 'visit_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );  
    
	register_sidebar( array(
		'name'          => 'Footer Left',
		'id'            => 'footer_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );  
    
	register_sidebar( array(
		'name'          => 'Footer Right',
		'id'            => 'footer_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );      
}
add_action( 'widgets_init', 'arphabet_widgets_init' );





// LOAD SCRIPTS
add_action( 'wp_enqueue_scripts', 'add_my_script' );
function add_my_script() {
    wp_enqueue_script(
        'my-scripts', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );    
    wp_enqueue_script(
        'my-scripts1', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/classie.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );        
    wp_enqueue_script(
        'my-scripts2', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/scripts.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );
}





// Changing excerpt length
function new_excerpt_length($length) {
return 16;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Changing excerpt more
function new_excerpt_more($more) {
return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');










// Add Custom Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
    	'utility_navigation' => __( 'Utility Navigation' )
    )
  );
} 
add_action( 'init', 'register_my_menus' );

function default_utility_nav() { // HTML markup for a default message in menu location
	echo "<ul class='nav'>					
		<li>Create the Utility Navigation</li>
	</ul>";
}


add_shortcode( 'divider', 'shortcode_insert_divider' );
function shortcode_insert_divider( ) {
 	return '<div class="divider"></div>';
}