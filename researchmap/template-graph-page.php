<?php
/*
Template Name: Data Display Graph
*/
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content" style="position:relative; min-height:600px;">

<div class="col-md-12 dmbs-main">
<?php get_sidebar("sidebarFilter"); ?>
    
    <div class="col-md-12 dmbs-main" style="min-height:600px;">

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header waypoint"><?php the_title() ;?>GRAPH TEMPLATE TEST</h2>
            <?php the_content(); ?>
            <?php wp_link_pages(); ?>
            <?php comments_template(); ?>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

    <?php get_sidebar("sidebarData"); ?>

</div>
<!-- end content container -->
      
            


<?php get_footer(); ?>