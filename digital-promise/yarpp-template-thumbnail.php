<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: mitcho (Michael Yoshitaka Erlewine)
*/ ?>
<h3 class="related-title">Related Articles</h3>
<?php if (have_posts()):?>

    
	<?php while (have_posts()) : the_post(); ?>
    <aside>    
        <div class="callout">
            <a href="<?php the_permalink(); ?>" rel="bookmark" class="callout-link">
                <?php if (has_post_thumbnail()):?>
                <div><?php the_post_thumbnail('large'); ?></div>
                <?php endif; ?>
            <h3><?php the_title() ;?></h3>
            </a>
        </div>
    </aside>
	<?php endwhile; ?>
    

<?php else: ?>
<p>No related articles</p>
<?php endif; ?>
