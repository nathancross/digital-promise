

</div>
<!-- end main container -->
</div>
<!-- end outer wrapper -->

<div class="visit-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 visit-left"><?php dynamic_sidebar( 'visit_left' ); ?></div>
            <div class="col-md-6 visit-right"><?php dynamic_sidebar( 'visit_right' ); ?></div>
        </div>
    </div>
</div>


<div class="container">
    <div class="dmbs-footer">

        <?php get_template_part('template-part', 'footernav'); ?>
        
        <div class="row">
            <div class="col-md-6 footer-left"><?php dynamic_sidebar( 'footer_left' ); ?></div>
            <div class="col-md-6 footer-right"><?php dynamic_sidebar( 'footer_right' ); ?></div>
        </div>
    </div>

    <?php wp_footer(); ?>
</div>    

</body>
</html>