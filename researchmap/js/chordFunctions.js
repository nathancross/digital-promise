function goChord(){
	console.log("gets here 1");
	
	var outerRadius = 600 / 2,
		innerRadius = outerRadius - 100;
	
	var fill = d3.scale.category20c();
	
	var chord = d3.layout.chord()
		.padding(.04)
		.sortSubgroups(d3.descending)
		.sortChords(d3.descending);
	
	var arc = d3.svg.arc()
		.innerRadius(innerRadius)
		.outerRadius(innerRadius + 20);
	
	var svg = d3.select("#ChordHolder").append("svg")
		.attr("width", outerRadius * 2.5)
		.attr("height", outerRadius * 2.5)
		.attr("style","overflow:visible;")
	  .append("g")
		.attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");
	console.log("gets here 2");
	
	d3.json('http://site1.dp-wp.beancreative.com/wp-content/uploads/sites/2/2015/12/clusters.txt',
		function(data) {
	
			var matrix = [];
			var indexByName = {};
			var nodes = [];
	
	
			//var nodeList = data.nodes.filter(function(n){return (n.group === 5)})
			 var nodeArrayf = data.nodes.filter( function(d) { return ((keep === undefined) && (level_option === 'TOP') && (d.type === "top")) || ((d.type === "subtop") && (d.id_top === keep))  || ((keep === undefined) && (level_option === 'SUB') && (d.type === 'subtop') ); })
   			 maxNodeSize = Math.max.apply( null, nodeArrayf.map( function(n) {return n.size;} ) );
   			 var nodeList = nodeArrayf.filter( function(d) {return (d.size >= minS*minS*maxNodeSize) }); 
	
			nodeList.forEach(function(n){
				indexByName[n.name] = nodes.length;
				var nodeInfo = {};
				nodeInfo.name = n.name;
				nodeInfo.label = n.label;
				nodeInfo.size = n.size;
				nodes.push(nodeInfo);
			})
	
			for (var i = 0; i < nodes.length;i++){
				matrix[i] = [];
			}
	
			var linksList = data.links.filter(function(n){
				return (indexByName[n.source] > -1 && indexByName[n.target] > -1);
			})
	
			linksList.forEach(function(n){
				matrix[indexByName[n.source]][indexByName[n.target]] = n.weight;
				matrix[indexByName[n.target]][indexByName[n.source]] = n.weight;
			})
	
			for (var i = 0; i < nodes.length;i++){
				var stretch = "["
				for (var j = 0; j < nodes.length;j++){
					if (!matrix[i][j])
						matrix[i][j] = 0;
	
					stretch +=","+matrix[i][j];
				}
				stretch += "]"
				console.log(stretch);
			}
	
	
	
			 chord.matrix(matrix);
	
			   var g = svg.selectAll(".group")
				  .data(chord.groups)
				.enter().append("g")
				  .attr("class", "group");
	
			  g.append("path")
				  .style("fill", function(d) { return fill(d.index); })
				  .style("stroke", function(d) { return fill(d.index); })
				  .attr("d", arc);
	
			   g.append("text")
				  .each(function(d) { d.angle = (d.startAngle + d.endAngle) / 2; })
				  .attr("dy", ".3em")
				  .attr("transform", function(d) {
					return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
						+ "translate(" + (innerRadius + 26) + ")"
						+ (d.angle > Math.PI ? "rotate(180)" : "");
				  })
				  .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
				  .text(function(d) { return nodes[d.index].label });
	
			  svg.selectAll(".chord")
				  .data(chord.chords)
				.enter().append("path")
				  .attr("class", "chord")
				  .style("stroke", function(d) { return d3.rgb(fill(d.source.index)).darker(); })
				  .style("fill", function(d) { return fill(d.source.index); })
				  .attr("d", d3.svg.chord().radius(innerRadius));
	
				
	
	
		});
	d3.select(self.frameElement).style("height", outerRadius * 2 + "px");
}

function D3okChord() {
  
  d3.select("#search").html('<cc>Search:</cc><br/><input type="text" name="search" id="MYsearch" onkeypress="doSearch(event)"" placeholder="Search authors or keywords..." class="empty"/><span id="SearchRes" class="SearchRes"></span><button class="emptyb" onclick="resetsearch()">Clear search</button>');

  // PREP html string
  // ...fields or subfields
  thehtml = '<'+'div id="level" class="level"><b style="margin-right:5px;">Select Level:</b><br/>'
  thehtml += '<span id="TOP" onclick="level_option=\'TOP\';update()">Topics</span>'
  thehtml += '<span id="SUB" onclick="level_option=\'SUB\';update()">Subtopics</span><br/><'+'p style="height:0px;""></'+'p>'
  // ...fixed or live
  thehtml += '<b style="margin-right:5px;">Select Layout:</b><br/>'
  thehtml += '<span id="FORCE" onclick="layout_option=\'FORCE\';updateA()">Dynamic</span>'  
  thehtml += '<span id="FIXED" onclick="layout_option=\'FIXED\';updateA()">Static</span></'+'div>'
  

  // ... add controls in the html page
  d3.select("#level").html(thehtml);

  // plot the network
  update();

}// end of D3ok()