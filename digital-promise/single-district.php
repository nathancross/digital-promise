<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<?php
//******************************************************* //
// ***************** B E G I N  K E Y ******************* //
$thisGMAPIkey = 'AIzaSyCuO2mbqOelxZV1koLmueKsjNT2jNOVGDE';
// ******************* E N D   K E Y ********************* //
// ******************************************************* //
?>

<!-- start content container -->
<div class="row dmbs-content">
    
    <?php if ( has_post_thumbnail() ) : ?>
       <?php the_post_thumbnail("full"); ?>
        <div class="homepage-feature">
            <div class="clear"></div>
        </div>
    <?php endif; ?>
    
    <div class="col-md-12 breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
        <?php if(function_exists('bcn_display'))
        {
            bcn_display();
        }?>
    </div>


    <div class="col-md-9 dmbs-main" style="padding-top:40px;">
        

        <?php

            //if this was a search we display a page header with the results count. If there were no results we display the search form.
            if (is_search()) :

                 $total_results = $wp_query->found_posts;

                 echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

                 if ($total_results == 0) :
                     get_search_form(true);
                 endif;

            endif;

        ?>

            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>
        
                        <div style="margin-left:0; margin-bottom:30px;" class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells callout district-header no-link">

                            <div class="Grid-cell u-large-1of2 u-med-1of2 u-small-full">
                                <div class="wrapper">
                                    <h1 style="padding:10px 15px 0; margin:0;"><?php the_title() ;?></h1>
                                    <p style="padding:0 15px;"><?= wpautop(get_post_meta( $post->ID, 'district_address', true)); ?></p>
                                    <p style="padding:0 15px;">
                                        <?php
                                        // if website is not empty, show icon and link
                                        if (!empty(get_post_meta( $post->ID, 'district_website', true))) {
                                        ?>
                                            <a class="blue" target="_blank" href="<?= get_post_meta( $post->ID, 'district_website', true); ?>" target="_blank"><i class="fa fa-globe fa-2x"></i></a> 
                                        <?php                            
                                        }                            
                                        ?>
                                        <?php
                                        // if facebook is not empty, show icon and link
                                        if (!empty(get_post_meta( $post->ID, 'district_facebook', true))) {
                                        ?>
                                            <a class="blue" target="_blank" href="<?= get_post_meta( $post->ID, 'district_facebook', true); ?>" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a> 
                                        <?php                            
                                        }                            
                                        ?>
                                        <?php
                                        // if twitter is not empty, show icon and link
                                        if (!empty(get_post_meta( $post->ID, 'district_twitter', true))) {
                                        ?>
                                            <a class="blue" target="_blank" href="<?= get_post_meta( $post->ID, 'district_twitter', true); ?>" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a> 
                                        <?php                            
                                        }                            
                                        ?>
                                    </p>
                                </div>    
                            </div><!-- / end flexbox grid cell -->

                            <div class="Grid-cell u-large-1of2 u-med-1of2 u-small-full">
                                <div class="wrapper">
                                    <a href="/initiative/league-of-innovative-schools/districts/#map" border=0><div id="district_local_map" style="margin:-15px 0; width:100%; height:100%;">.</div></a>
                                </div>    
                            </div><!-- / end flexbox grid cell -->

                        </div><!-- /end grid container -->
        
        
                        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">

                            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                                <div class="wrapper">
                                    <div class="district-data-display"><p><?= number_format(get_post_meta( $post->ID, 'number_of_students', true), 0, '', ','); ?></p></div>
                                    <div class="district-data-label"><p>Number of Students</p></div>
                                </div>
                            </div><!-- / end flexbox grid cell -->

                            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                                <div class="wrapper">
                                    <div class="district-data-display"><p><?= get_post_meta( $post->ID, 'percent_free_and_reduced_lunch', true); ?>%</p></div>
                                    <div class="district-data-label"><p>Free and Reduced Lunch</p></div>
                                </div>
                            </div><!-- / end flexbox grid cell -->

                            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                                <div class="wrapper">
                                    <div class="district-data-display"><p><?= get_post_meta( $post->ID, 'percent_students_with_access_to_high_speed_broadband', true); ?>%</p></div>
                                    <div class="district-data-label"><p>Students with Access to High Speed Broadband</p></div>
                                </div>
                            </div><!-- / end flexbox grid cell -->

                            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-1of2">
                                <div class="wrapper">
                                    <div class="district-data-display"><p><?= get_post_meta( $post->ID, 'percent_students_k-12_with_access_to_personal_school-provided_device', true); ?>%</p></div>
                                    <div class="district-data-label"><p>K-12 Students with Access to a Personal School-provided Device (1:1)</p></div>
                                </div>
                            </div><!-- / end flexbox grid cell -->

                        </div><!-- /end grid container -->
        
        
        <h2 class="divider orange">Learn from this district about:</h2>
        
                            <div style="padding:0 15px"><?php the_content(); ?></div>
        <hr>
        
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">

                            <div class="Grid-cell u-large-1of2 u-med-full u-small-full">
                                <div class="callout superintendent no-link">
                                    <div class="superintendent-photo"><?= wp_get_attachment_image ( get_post_meta( $post->ID, 'superintendent_headshot', true), '', false, '' ); ?></div>
                                    <div class="superintendent-info">
                                        <h3 style="margin-top:0;"><?= get_post_meta( $post->ID, 'superintendent_name', true); ?></h3>
                                        <p class="superintendent-label">Superintendent</p>
                                        <?php
                            			// if twitter is not empty, show icon and link
                            			if (!empty(get_post_meta( $post->ID, 'superintendent_twitter', true))) {
                            			?>
                            				<p style="padding:0 15px;"><a target="_blank" class="blue" href="<?= get_post_meta( $post->ID, 'superintendent_twitter', true); ?>" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a></p>
                            			<?php                            
                            			}                            
                            			?>
                                    </div>
                                    <?php
                                    // if other leaders is not empty, show list
                                    if (!empty(get_post_meta( $post->ID, 'other_district_leaders_names', true))) {
                                    ?>
                                        <p style="padding:15px 15px 0;" class="district-leaders"><strong>Other District Leaders: </strong><?= get_post_meta( $post->ID, 'other_district_leaders_names', true); ?></p> 
                                    <?php                            
                                    }                            
                                    ?>
                                </div>
                            </div><!-- / end flexbox grid cell -->

                            <div class="Grid-cell u-large-1of2 u-med-full u-small-full">
                                <div class="callout district-information no-link">
                                    <div class="district-work">
                                        <h3 style="margin-top:0; padding:0;">District Work</h3>
                                        <?php
										foreach((get_the_category()) as $category) {
											echo '<a href="/initiative/league-of-innovative-schools/districts/?tcn=' . urlencode($category->cat_name) . '">' . $category->cat_name . '</a><br/>';
										}
										?>
                                    </div>
                                    <div class="district-geographic">
                                        <h3 style="margin-top:0;">Geographic Area</h3>
                                        <p><?= get_post_meta( $post->ID, 'district_locale', true); ?></p>
                                    </div>
                                    
                                </div>
                            </div><!-- / end flexbox grid cell -->            

        </div><!-- /end grid container -->

<!-- ******************************************************* -->
<!-- ***************** B E G I N   M A P ******************* -->
                                                     
<script>

function initMap() {
	var thisCenter = new google.maps.LatLng(<?= get_post_meta( $post->ID, '_wpgmp_metabox_latitude', true); ?>, <?= get_post_meta( $post->ID, '_wpgmp_metabox_longitude', true); ?>);

	map = new google.maps.Map(document.getElementById('district_local_map'), {
		center: thisCenter,
		zoom: 8,
		minZoom: 8,
		maxZoom: 8,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false,
		styles: stylesArray,
		scrollwheel: false,
		draggable: false,
	});
	
	var myLatLng;
	var marker;
	
	var customMarker = {
    	url: '/wp-content/uploads/2016/03/MapPinSmall.png',
    	size: new google.maps.Size(24, 36),
    	origin: new google.maps.Point(0, 0),
    	anchor: new google.maps.Point(12, 36)
    };
	
	myLatLng = {lat: <?= get_post_meta( $post->ID, '_wpgmp_metabox_latitude', true); ?>, lng: <?= get_post_meta( $post->ID, '_wpgmp_metabox_longitude', true); ?>};
	marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: customMarker,
			clickable: false,
	});
	

}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $thisGMAPIkey; ?>&callback=initMap" async defer></script>
                            
<!-- ******************* E N D   M A P ********************* -->
<!-- ******************************************************* -->
        
        <!-- the_meta(); -->
        
        

                     <?php  endif; ?>

                <?php endwhile; ?>
                <?php posts_nav_link(); ?>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>

   </div>

    <?php //get the right sidebar ?>
   <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

