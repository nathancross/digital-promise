<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">
    
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/" style="display:none;">
    	<?php if(function_exists('bcn_display'))
    	{
        	bcn_display();
    	}?>
	</div>

        <?php

            //if this was a search we display a page header with the results count. If there were no results we display the search form.
            if (is_search()) :

                 $total_results = $wp_query->found_posts;

                 echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

                 if ($total_results == 0) :
                     get_search_form(true);
                 endif;

            else:
                echo "<h2 class='page-header'>Our Blog</h2>";

            endif;

        ?>
        
        <?php
        
        //echo $_SERVER["REQUEST_URI"];
        
        if ($_SERVER["REQUEST_URI"] === "/our-blog/") {
        
        ?>
        
        <h2>Featured Stories</h2>

		<div class="featured-stories Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
        <?php 
        
		$max_to_show = 3;
		$number_shown = 0;        
        
        $args = array(
        	'post_type' => 'post', 
        	'posts_per_page' => 2, 
        	'orderby' => 'date', 
        	'order' => 'DESC',
        	'post_status' => 'publish',
        	'nopaging' => true,
   	        'meta_query' => array(
   	         	array(
   	         		'key' => 'is_featured',
   	         		'value' => true,
   	         		'compare' => '='
   	         		)
   	         ),
        );
        
        
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); 
        
        	$number_shown ++;
        	if ($number_shown <= $max_to_show) {
        ?>
                       
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                        
                        
                        <?php 
                        echo '
                                <a href="' . get_permalink(get_the_ID()) . '" class="callout-link">';
                                if(get_the_post_thumbnail()) {
                                        $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size = 'large', $icon = false );
                                        echo '<img src="' . $image_attributes[0] . '">';
                                    }

                                echo            '<h3>' . get_the_title() . '</h3>
                                            <p>' . get_the_excerpt() . '</p>
                                </a>
                            ';
                        ?>

                       </div>
                    </div>

            </div>

		<?php 
			
			}
			
		endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div><!-- /end grid container -->

        <?php
        
       	}
        
        ?>


    </div>
</div>

<div class="row dmbs-content" style="margin-bottom:20px;">

    <div class="col-md-2">
    	
    	<?php if ($_SERVER["REQUEST_URI"] === "/our-blog/") { ?>
    
        <h2 style="margin-bottom:0;">Recent Posts</h2>
        
        <?php } else { ?>
    
        <h2 style="margin-bottom:0;">Posts</h2>
        
        <?php } ?>    </div>
    
    <div class="col-md-10">
        
        
        <!-- display archives dropdown -->
		<!-- determine the current sector to display -->
		<?php  $this_arcdd = get_query_var( 'arcdd', false );  ?>

        <form style="float:right; margin-left:30px; margin-top:2.25rem;" id="archive-select" class="archive-select" action="/our-blog/" method="post">

            <?php
            $which_selected = $_SERVER["REQUEST_URI"];
            
            $args = array(
				'type'             => 'monthly',
				'limit'            => '', 
				'show_post_count'  => false,
				'echo'			   => 0,
				'order'            => 'DESC',
				'format'           => 'option', 
        		'post_type'        => 'post',
            );
            
            //echo "..." . $_SERVER["REQUEST_URI"] . "...";
            ?>

            <?php $select  = wp_get_archives( $args ); ?>
            
            <?php $replace = get_site_url(); ?>
            
            <?php $select  = str_replace( $replace, '', $select ); ?>
            
            <?php $replace = "<option value='" . $which_selected . "'"; ?>

            <?php $select  = str_replace( $replace, $replace . ' selected', $select ); ?>
            
			<label for='arcdd'>Archives:</label> <select name="arcdd" id="arcdd" class="postform" onchange="this.form.action = document.getElementById('arcdd').options[document.getElementById('arcdd').selectedIndex].value; return this.form.submit();">
			<option value='/our-blog/'> Select Archive Month </option>

            <?php echo $select; ?>
            
            </select>

            <noscript>
                <input type="submit" value="View" />
            </noscript>

        </form>
   
		<!-- determine the current sector to display -->
		<?php  $this_cat = get_query_var( 'cat', 0 );  ?>
        

        <form style="float:right; margin-top:2.25rem;" id="category-select" class="category-select" action="/our-blog/" method="get">

            <?php
            $which_selected = 0;
            if ($this_cat > 0) {
            	$which_selected = $this_cat;
            }
            
            $args = array(
                'show_option_all' => __( 'Show all categories' ),
                'orderby'          => 'name',
                'echo'             => 0,
                'show_count'	   => 0,
                'hide_empty'	   => 1,
                'hierarchical'	   => 1,
                'exclude_tree'	   => '1,2,5,6,7,9,19,20,25,76,77',
                'selected'		   => $which_selected,
            );
            ?>

            <?php $select  = wp_dropdown_categories( $args ); ?>
            <?php $replace = "<label for='cat'>Categories:</label> <select$1 onchange='return this.form.submit()'>"; ?>
            <?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

            <?php echo $select; ?>

            <noscript>
                <input type="submit" value="View" />
            </noscript>

        </form>
        


    </div>
</div>

<div class="row dmbs-content">
    <div class="col-md-12">
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post (unused in this template)
                    if ( is_single() ) : ?>
                        
                    <?php
                    // list of posts
                    else : ?>
                    
            <?php
            //echo "is_featured = ";
			$is_featured = get_post_meta($post->ID, 'is_featured', TRUE);
			//if(!$is_featured) {
			?>
			
			<div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                        
                        
                        <?php 
                        echo '
                                <a href="' . get_permalink(get_the_ID()) . '" class="callout-link">';
                                if(get_the_post_thumbnail()) {
                                        $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size = 'large', $icon = false );
                                        echo '<img src="' . $image_attributes[0] . '">';
                                    }

                                echo '<p class="byline vcard" style="margin-bottom:1em;"><time class="updated" datetime="' . get_the_date('Y-m-d') . '" itemprop="datePublished">' . get_the_date() . '</time></p>
                                <h3>' . get_the_title() . '</h3>
                                <p>' . get_the_excerpt() . '</p>
                                </a>
                            ';
                        ?>

                       </div>
                    </div>

            </div>

			<?php
			//}
			?>

				
                     <?php  endif; ?>

                <?php endwhile; ?>
            
        </div><!-- /end grid container -->
            <?php if(function_exists('wp_paginate')) {
                wp_paginate();
                }
                else {
                    posts_nav_link();
                }
            ?> 
            
            <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>
    </div>
   </div>
<!-- end content container -->

<?php get_footer(); ?>

