<div id="sidebar1" class="slide sidepanel sidebar large-4 medium-4 columns" role="complementary">
<div id="title">
      Digital Promise<br/>
      Research Map<br/>  

    </div> 

    <div id="subtitle">    
      <font size= "4"> This map provides an overview of research topics related to education. </font>
    </div>
    <div id="legend">
      <br/>

      <cc>LEGEND:</cc><br/><br/>

      <div class="line">
        <svg height="14" width="15"><circle cx="8" cy="7" r="7" fill="lightgreen" /></svg>
        <b>Circles</b> represent research topics. Circle <i>size</i> is proportional to the number of publications on that topic.
      </div>

      <div class="line">
        <svg height="15" width="15"><line x1="3" y1="15" x2="14" y2="1" style="stroke: #B2D9D8;stroke-width:3" /></svg>
        <b>Lines</b> represent connections between topics. Line <i>thickness</i> is proportional to the similarity of two topics.
      </div>  

      <div class="line">
        <svg height="15" width="15">
          <polygon points="8,8,8,16,15,12" style="fill:magenta;" />
          <polygon points="8,8,15,12,15,4" style="fill:royalblue;" />
          <polygon points="8,8,15,4,8,0" style="fill:lightgreen;" />
          <polygon points="8,8,8,0,1,4" style="fill:yellow;" />
          <polygon points="8,8,1,4,1,12" style="fill:orange;" />
          <polygon points="8,8,1,12,8,16" style="fill:red;" />
        </svg> 
        <b>Colors</b> represent research topics.
      </div>
	<div id="level"></div>
      <div id="search" class="cf">shoop
      </div>
    </div>
    <div id="filters"></div>
    </div>