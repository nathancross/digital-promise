<?php /* Template Name: Wide page with no sidebars */ ?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-9">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
   		 	<?php if(function_exists('bcn_display'))
    		{
       			bcn_display();
		    }?>
		</div>
    </div>
    <div class="col-md-3 dmbs-right">
        <aside id="addthis-widget-2" class="widget atwidget">
            <h3>Share:</h3>
            <?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'small_toolbox'); ?>
        </aside>
        
    </div>
</div>
 
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">        

        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header waypoint"><?php the_title() ;?></h2>
            <?php the_content(); ?>
            <?php wp_link_pages(); ?>
            <?php comments_template(); ?>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>
