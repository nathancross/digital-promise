<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-9 dmbs-main">
    
    	<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
   		 	<?php if(function_exists('bcn_display'))
    		{
       			bcn_display();
		    }?>
		</div>

        <?php

            //if this was a search we display a page header with the results count. If there were no results we display the search form.
            if (is_search()) :

                 $total_results = $wp_query->found_posts;

                 echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

                 if ($total_results == 0) :
                     get_search_form(true);
                 endif;

            endif;

        ?>

            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>

                            <?php if ( has_post_thumbnail() ) : ?>
                                <?php the_post_thumbnail( 'full' ); ?>
                                <div class="clear"></div>
                            <?php endif; ?>
                            
                            <h2 class="page-header"><?php the_title() ;?></h2>
                            
                            <p class="byline vcard" style="margin-bottom:1em;">
                                <?php
                                    printf( __( '<time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> | by', 'devdmbootstrap3' ), get_the_time( 'Y-m-j' ), get_the_time(get_option('date_format')), get_the_author_link( get_the_author_meta( 'ID' ) ), get_the_term_list( $post->ID, 'custom_cat', ' ', ', ', '' ) );
                                ?>
                                <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
                            </p>
                            
                            <?php the_content(); ?>
                            <!-- categories -->
                            <div class="category-list"><?php printf( __( 'Category', 'devdmbootstrap3' ).': %1$s', get_the_category_list(', ') ); ?></div>
                            <?php wp_link_pages(); ?>
                            <!-- comments area -->
                            <?php comments_template(); ?>

                        </div>
                    

                     <?php  endif; ?>

                <?php endwhile; ?>
                <?php posts_nav_link(); ?>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>

   </div>

   <?php //get the right sidebar ?>
   <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

