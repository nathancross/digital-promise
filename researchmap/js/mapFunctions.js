
var toggleDiv = undefined;

// layout, levels & search stuff
var level_option = 'TOP'
var layout_option = 'FORCE'
var keep = undefined;
var stuff_to_search = undefined;

var graph_type = "FORCE";

var selectedNode;

var myTiles;



// Different tuning variables
var TOPminX=-910,
    TOPmaxX=710,
    TOPminY=-900,
    TOPmaxY=990,
    SUBminX=-4200,
    SUBmaxX=3200,
    SUBminY=-4000,
    SUBmaxY=3000,
    GENmaxNodeSize = 14000,
    SSSmaxNodeSize = 3000,
    TOPmaxLinkWeight = 0.00013,
    SUBmaxLinkWeight = 0.007;   
var WIDTH = 0;
var HEIGHT = 0;
var TOPforce=[.4,-6000,150],
    SUBforce=[.5,-500,20];        

/*-------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------*/

// Get the current size & offset of the browser's viewport window
function getViewportSize( w ) {
	
	/*return{
		w: jQuery(".entry-content").width(), 
          h: jQuery(".entry-content").height(),
          x : 0,
          y : 0
	}*/
    var w = w || window;
    if( w.innerWidth != null )
	return{
		w:jQuery("#inner-content").width(),
		h:jQuery(window).height() - jQuery(".dmbs-footer").outerHeight(),
		x:0,
		y:-500,
		r:"mine!"
	};
      return { w: w.innerWidth, 
          h: w.innerHeight,
          x : w.pageXOffset,
          y : w.pageYOffset,
		  r:"a" };
    var d = w.document;
    if( document.compatMode == "CSS1Compat" )
      return { w: d.documentElement.clientWidth,
          h: d.documentElement.clientHeight,
          x: d.documentElement.scrollLeft,
          y: d.documentElement.scrollTop,
		  r:"b" };
    else
      return { w: d.body.clientWidth, 
          h: d.body.clientHeight,
          x: d.body.scrollLeft,
          y: d.body.scrollTop,
		  r:"c"};
  }
  
  jQuery(function(){
	  jQuery.post("/wp-admin/admin-ajax.php",{action:'get_user_tiles'},function(results){
	  	if (results != "LOGGEDOUT"){
			myTiles = JSON.parse(results);
			
			for (var key in myTiles){
				var op = jQuery("<option>").attr("value",key).html(key);
				jQuery("#saverGroupSelector").append(op);
			}
			var op = jQuery("<option>").attr("value","NEW").html("Create New Group");
			jQuery("#saverGroupSelector").append(op);
		}
		else
			myTiles = results;
			
		 console.log(myTiles);
	  });
  });


/*-------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------*/

// Do the stuff -- to be called after D3.js has loaded
function D3ok() {
	jQuery(".popup").hide();
	
	if (graph_type == "CHORD")
		jQuery(".forceKey").hide();
	else
		jQuery(".chordKey").hide();
  jQuery(".page-header").hide();
  jQuery("#saverClose").click(function(){jQuery(".popup").fadeOut(500);});
  jQuery("#inner-content").css({position:"relative",width:jQuery(window).width(),left:jQuery("#inner-content").offset().left*-1});
  jQuery(window).resize(function(){
	  jQuery("#inner-content").css({left:0});
	  jQuery("#inner-content").css({position:"relative",width:jQuery(window).width(),left:jQuery("#inner-content").offset().left*-1});
  })
  d3.select("#search").html('<cc>Search:</cc><br/><input type="text" name="search" id="MYsearch" onkeypress="doSearch(event)"" placeholder="Search authors or keywords..." class="empty"/><br><span id="SearchRes" class="SearchRes"></span><button class="emptyb" onclick="clickSearch(event)">Search</button><br><button class="emptyb fliptop" onclick="flipSearch(event)">Search '+(level_option =="TOP"?"Subtopics":"Topics")+'</button><button class="emptyb" onclick="resetsearch()">Clear search</button>');

 // PREP html string
  // ...fields or subfields
  thehtml = '<'+'div id="level" class="level"><b style="margin-right:5px;">Select Level:</b><br/>'
  thehtml += '<select id="levelSelect" onchange = levelChanged()><option id="TOP" value="TOP">Topics</option><option id="SUB" value="SUB">Subtopics</option></select>'
  //thehtml += '<span id="TOP" onclick="level_option=\'TOP\';update()">Topics</span>'
  //thehtml += '<span id="SUB" onclick="level_option=\'SUB\';update()">Subtopics</span><br/><'+'p style="height:0px;""></'+'p>'
  // ...fixed or live
  
  if (graph_type === "FORCE"){
	  thehtml += '<b style="margin-right:5px;">Select Layout:</b><br/>'
	  /*thehtml += '<span id="FORCE" onclick="layout_option=\'FORCE\';updateA()">Dynamic</span>'  
	  thehtml += '<span id="FIXED" onclick="layout_option=\'FIXED\';updateA()">Static</span></'+'div>'*/
	  thehtml += '<select id="styleSelect" onchange = styleChanged()><option id="FORCE" value="FORCE">Dynamic</option><option id="FIXED" value="FIXED">Static</option></select></div>'
  }

  
 d3.json('/wp-content/themes/researchmap/data/defaultVAR.json',
    function(data) {    
      // set some variables for sizes
      TOPmaxNodeSize = data.TOPmaxNodeSize
      TOPmaxLinkWeight = data.TOPmaxLinkWeight
      TOPWrange = data.TOPWrange
      SUBmaxNodeSize = data.SUBmaxNodeSize
      SUBmaxLinkWeight = data.SUBmaxLinkWeight 
      SUBWrange = data.SUBWrange   
      //set zoom threshold variables
      TOPshowthr=data.TOPshowthr
      SUBshowthr=data.SUBshowthr
      // set some variables for positions
      TOPforce = data.TOPforce
      TOPpositions = data.TOPpositions
      SUBforce = data.SUBforce
      SUBpositions = data.SUBpositions
      // put that command in this loop to ensure the parameter have been taken into account
      update();
  })
  

  // ... add controls in the html page
  d3.select("#level").html(thehtml).attr("class","level");

  // plot the network
  update();

}// end of D3ok()

function levelChanged(){
	
	console.log("Changed the topic level");
    var e = document.getElementById("levelSelect");
    level_option = e.options[e.selectedIndex].value;
	layoutChanged = false;
	update();
  }
  
  function styleChanged(){
    var e = document.getElementById("styleSelect");
	console.log("Changed the layout style");
	layoutChanged = true;
    layout_option = e.options[e.selectedIndex].value;
	updateA();
  }
/*-------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------*/

function resetsearch() {
  stuff_to_search=undefined;
  d3.select('#MYsearch').attr('value',"");
  jQuery("#SearchRes").html("");
  jQuery("#MYsearch").val("");
  update();
 // D3ok()
}

function jumpBarChanged(){
	console.log("jump");
	var bar = jQuery(".jumpBar");
	var c = bar.val();
	var datH = jQuery("#dataHolder");
	var targ = jQuery("."+c,datH);
	
	var offset = targ.offset().top;
	
	jQuery("#dataHolder").animate({
          scrollTop: offset -jQuery("#sidebarData").offset().top - 20
        }, 500);
	
}

var layoutChange = false;


function update() {
  // put back keep to undefined: by default we do not focus on a single field
  if (!layoutChange)
  	keep=undefined;

  //...FILTERS
  thehtml = '<cc>Filters:</cc><br/><br>'
  // ... node size filter
  thehtml += "<cc>filter for relative size of topic</cc><br><br>"
  thehtml += '<'+'div class="sliderS"><svg style="float:left;stroke: #000000;stroke-width:1;" height="18" width="18"><circle cx="9" cy="9" r="4" fill="lightgreen" stroke /></svg>'
  if (level_option === 'TOP' || graph_type != "CHORD") {thehtml += '<input type="range" id="Srange" style="float:left;background-color:#DDDDDD;" min="0" max="1" value="0" step="0.01" onchange="updateA()" />'}
  else{thehtml += '<input type="range" id="Srange" style="float:left;background-color:#DDDDDD;" min="0" max="1" value="0.5" step="0.01" onchange="updateA()" />'}
  thehtml += '<svg style="float:left;stroke: #000000;stroke-width:1;" height="18" width="18"><circle cx="9" cy="9" r="9" fill="lightgreen" /></svg></'+'div>'
  // ... link weight filter
  thehtml += "<br><cc>filter for relative strength of topic similarity</cc><br><br>"
  thehtml += '<div style="clear:both"></div><'+'div><svg style="float:left;" height="18" width="18"><line x1="3" y1="17" x2="16" y2="1" style="stroke: #325958;stroke-width:1"  /></svg>'
  if (level_option === 'TOP') { thehtml += '<input type="range" id="Wrange" style="float:left;background-color:#DDDDDD;" min="-6" max="-3.95" value="-6" step="0.01" onchange="updateA()"/>'}
  else { thehtml += '<input type="range" style="cursor:ew-resize;" id="Wrange" style="float:left;background-color:#DDDDDD;" min="-6" max="-2.5" value="-4" step="0.01" onchange="updateA()"/>'}
  thehtml += '<svg style="float:left;" height="18" width="18"><line x1="3" y1="17" x2="16" y2="1" style="stroke: #325958;stroke-width:5"  /></svg></'+'div><div style="clear:both"></div>'
  d3.select("#filters").html(thehtml);
  
  jQuery(".fliptop").html("Search "+(level_option == "TOP"?"Subtopics":"Topics"));
  

  // plot the network
  updateA();

}// end of update

/*-------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------*/

  function doSearch(e){
	  
    if(e.keyCode === 13){
		
        stuff_to_search = document.getElementById("MYsearch").value;
        stuff_to_search = stuff_to_search.toLowerCase();
        if (stuff_to_search === ''){stuff_to_search=undefined}
        updateA()
    }
  }
  
   function clickSearch(e){
	  

		
        stuff_to_search = document.getElementById("MYsearch").value;
        stuff_to_search = stuff_to_search.toLowerCase();
        if (stuff_to_search === ''){stuff_to_search=undefined}
        updateA()
    
  }
  
  function flipSearch(e){
	  
   
   console.log("Changed the topic level");
    var e = jQuery("#levelSelect");
    e.val(level_option == "TOP"?"SUB":"TOP")
	level_option = level_option == "TOP"?"SUB":"TOP";
	layoutChanged = false;
	//update();
		
        stuff_to_search = document.getElementById("MYsearch").value;
        stuff_to_search = stuff_to_search.toLowerCase();
        if (stuff_to_search === ''){stuff_to_search=undefined}
        update()
    
  }
  
  function saveArticle(type,index){
	  console.log(type);
	  console.log(index);
	  
	  var pape = selectedNode[type][index];
							console.log(pape);
							fillSaver(pape);
  }
  function fillSaver(art){
	  var saver = jQuery("#itemSaver");
	  jQuery("#saverNodeName").html(art[0]);
	  jQuery("#saverNodeInfo").html(art[1]+"<br>"+art[2]+"("+art[3]+")<br><br>Citations:"+art[4]);
	  
	  jQuery("#saverComments").val("");
	  
	  jQuery(".popup").fadeIn(500);
	  
	  jQuery("#saverSaveButton").click(function(){
	  	saveNodeToTiles(art);
	  });
  }
  
  function saveNodeToTiles(art){
	  var group = jQuery("#saverGroupSelector").val();
	  
	  if (group == "NEW")
	  {
		  
			var count = 0;
			
			var grouplist = [];
			
			for (var ogroup in myTiles){
				grouplist.push(ogroup);
				count++;
			}
			
			var title = "My Group "+(++count);
			while (grouplist.indexOf(title) > 0){
				title = "My Group "+(++count);
			}
			myTiles[title] = [];
			group = title;
		
		
	  }
	  
	  var curTiles = myTiles[group];
	  
	  var has = false;
	  for (var i = 0; i < curTiles.length;i++){
		  if (curTiles[i][0] == art[0])
		  has = true;
	  }
	  if (!has){
	  	var nNode = art;
		nNode.push(jQuery("#saverComments").val());
		myTiles[group].push(nNode);
		
		var jsonTiles = JSON.stringify(myTiles);
		
		jQuery.post("/wp-admin/admin-ajax.php",{action:"save_user_tiles",Tiles:jsonTiles},function(results){
			console.log("tile saved");
			});
	  }
	  jQuery(".popup").fadeOut(500);
	  
	  
	  
  }

/*-------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------*/

function updateA(){
  // close info panel in case it was previously opened
  d3.select('div#clusterInfo').attr('class','panel_off');

  if (stuff_to_search === undefined) { 
    //d3.select('#MYsearch').attr('placeholder',"Search authors or keywords...")
    document.getElementById("MYsearch").innerHTML="";
    ;}

  // deal with level and layout options + parameters
    if (layout_option === 'FIXED' && graph_type != "CHORD"){
      //d3.select('#FIXED').attr('class','active')
	  document.getElementById("FIXED").selected = true;
      //d3.select('#FORCE').attr('class','')
    }  
    if (layout_option === 'FORCE' && graph_type != "CHORD"){
     // d3.select('#FIXED').attr('class','')
     // d3.select('#FORCE').attr('class','active')
	  document.getElementById("FORCE").selected = true;
    } 
    if (level_option === 'TOP'){
     // d3.select('#TOP').attr('class','active')
	  document.getElementById("TOP").selected = true;
     // d3.select('#SUB').attr('class','')
     // d3.select('span#force').attr('class','')
    }
    if (level_option === 'SUB'){
     // d3.select('span#TOP').attr('class','')
      //d3.select('span#SUB').attr('class','active')
	  document.getElementById("SUB").selected = true;
     // d3.select('span#force').attr('class','')
    } 
    if (keep !== undefined ){
      d3.select('span#TOP').attr('class','')
      d3.select('span#SUB').attr('class','')
      d3.select('#Wrange').attr('max',"-2.5");
    }    

  var minS=document.getElementById("Srange").value;
  var minW=Math.pow(10,document.getElementById("Wrange").value);
  

  // Some constants for zoom and adjust to screen size
  s = getViewportSize();

  

  if (s.w > s.h*8/6){
     WIDTH = s.h*8/6; 
    HEIGHT = s.h;
  }
  else {
     WIDTH = s.w; 
     HEIGHT = s.w*6/8;    
  }

  var SHOW_THRESHOLD = 1.5;

  // Variables keeping graph state
  var activeCluster = undefined;
  var currentOffset = { x : 0, y : 0 };
  var currentZoom = 1.0;
  
  // The D3.js scales
  var xScale = d3.scale.linear()
    .domain([0, WIDTH])
    .range([0, WIDTH]);
  var yScale = d3.scale.linear()
    .domain([0, HEIGHT])
    .range([0, HEIGHT]);
  var zoomScale = d3.scale.linear()
    .domain([0,6])
    .range([0,6])
    .clamp(true);

  //var color = d3.scale.category20().domain(d3.range(20));  
  var color = d3.scale.ordinal().range(["#D72F4A","#EF9326","#F2CC20","#3CB49D","#439AD1","#95a5a6","#86d37c","#4ecdc4","#f9bf3a","#f21957","#f9773e","#ea5475","#8bc34a","#81cfe0","#6b7a88","#1ab5fe","#2fcc71","#d2d7d3","#a1ded0","#fff578"]).domain(d3.range(20));

  /* ---------------------------------------------------------------------  */

  // Add to the page the SVG element that will contain the cluster network
  d3.select("#loading").remove()
  d3.select("#biblioMap").selectAll("svg").remove()
  var svg = d3.select("#biblioMap").append("svg")
    .attr('xmlns','http://www.w3.org/2000/svg')
    .attr("width", jQuery("#inner-content").width())
    .attr("height", HEIGHT)
    .attr("id","graph")
    .attr("viewBox", "0 0 " + WIDTH + " " + HEIGHT )
    .attr("preserveAspectRatio", "xMidYMid meet");
	
	d3.select("#chordArea").selectAll("svg").remove()
  var svg2 = d3.select("#chordArea").append("svg")
    .attr('xmlns','http://www.w3.org/2000/svg')
    .attr("width", jQuery("#inner-content").width())
    .attr("height", HEIGHT)
    .attr("id","graph")
    .attr("viewBox", "0 0 " + WIDTH + " " + HEIGHT )
    .attr("preserveAspectRatio", "xMidYMid meet");
	
	jQuery(window).resize(function(){
		
		s = getViewportSize();
		  
		
		  if (s.w > s.h*8/6){
			 WIDTH = s.h*8/6; 
			HEIGHT = s.h;
		  }
		  else {
			 WIDTH = s.w; 
			 HEIGHT = s.w*6/8;    
		  }
		
		svg.attr("width", jQuery("#inner-content").width())
    	.attr("height", HEIGHT);
		
		svg2.attr("width", jQuery("#inner-content").width())
   		 .attr("height", HEIGHT)
	});
	
	

  // Cluster panel: the div into which the cluster details info will be written
  clusterInfoDiv = d3.select("#clusterInfo");

  // Toggle function for both the Info Pane or the About page 
  toggleDiv = function( id, status ) {
    var d = d3.select('div#'+id);
    var b = d3.select('body');
    var hiddenDivs = ['#hmenu', '#sidepanel', '#level', '#clusterInfo'];

    if( status === undefined )
      status = d.attr('class') == 'panel_on' ? 'off' : 'on';

    if (status == 'on') {
      hiddenDivs.forEach(function(div) {
        d3.select(div).style('opacity',0.4);
      });
      b.style('overflow','hidden');
    }

    if (status == 'off') {
      hiddenDivs.forEach(function(div) {
        d3.select(div).style('opacity',1);
      });
      b.style('overflow','visible');
    }

    d.attr( 'class', 'panel_' + status ).style('opacity',1);
    //d3.select('div#close_about').style('opacity',1);
    d3.select('div#close_givefeedback').style('opacity',1);
    return false;
  }


  // Prep content for the Info panel with cluster details.
  function getclusterInfo( n, nodeArray ) {
	  
	  var decode = [
	    ["MRP","Most Representitive Paper"],
		["MCP","Most Cited Papers"],
		["MCAU","Most Cited Authors"],
		["stuffK","Most Frequent Keywords"],
		["stuffJ","Most Frequent Journals Sources"],
		["stuffS","Most Frequent subject categories"],
		["stuffR","Most Frequent References"]	
		
	]
      info = '<'+'div>'
      // general info
      info += '<'+'div class="stuff" style="margin-left:10px; margin-top:10px;font-size:22px"><b>About This Topic</b></'+'div>'
      whichlevel='topic'
      if (n.type === 'subtop'){whichlevel='subtopic'}
      lab=auxSTS(n.label,stuff_to_search);
      des=auxSTS(n.descr,stuff_to_search);
      info += '<'+'div class="stuff" style="font-size:18px;"><br/>The ' + whichlevel + ' <b>' + lab + '</b> contains '+ n.size + ' publications and includes research on ' + des + '<br/></'+'div><br/>'

		if (n.type != 'subtop')
			info += "<div class='learnMoreButton'>Learn More</div><br>";
			
		info+="<br>Jump To:<br>"
		
		//var jumpBar = jQuery("<select>"	).prop("onchange","jumpBarChanged()").prop("class","jumpBar");
		
		info += "<select class='jumpBar' onchange='jumpBarChanged()'>"
		for (var i = 0; i < decode.length;i++){
			var op = jQuery("<option>").prop("value",decode[i][0]).html(decode[i][1]);
			//jumpBar.append(op);
			info += op.prop("outerHTML");
		}
		
		//info += jumpBar.prop("outerHTML");
		info +="</select>";
	
	  info += '<'+'div class="MRP">' + articlesTables(n.MRP,'Most Representative Papers on this Topic',"MRP") + '</'+'div>'
      // most cited articles
      info += '<'+'div class="MCP">' + articlesTables(n.MCP,'Most Cited Papers on this Topic',"MCP") + '</'+'div>'
      // most cited authors
      info += '<'+'div class="MCAU">' + authorsTables(n.MCAU,'Most Cited Authors on this Topic') + '</'+'div>'
      // most used items
      info += '<'+'div class="stuff" style="font-size:18px;"><br/><b>Additional Information on this Topic</b></'+'div>'
      info += '<'+'div class="stuff"><b>f(%)</b> indicates how <b>frequently</b> an item (keyword, author, etc.) appears in this topic. <b>&sigma;</b> indicates how unique or <b>significant</b> an item is to this particular topic, where "&plus;" means that item appears more frequently in this topic than it does across all topics, "=" means that item appears at approximately the same frequency in this topic and across all topics, and "&minus;" mean that item appears less frequently in this topic than it does across all topics.</'+'div><br/>' 

      
      info += itemsTables(n.stuffK, 'Keyword','Most Frequent Keywords','stuffK')
      info += itemsTables(n.stuffJ, 'Journal','Most Frequently Published in journals','stuffJ')
      info += itemsTables(n.stuffS, 'Subject','Most Frequent subject categories','stuffS')
      info += itemsTables(n.stuffR, 'Reference','Most frequently used references','stuffR')       
      // end
      info+='</'+'div>'
    return info;
  }

  //---------------------
  function auxSTS(ee,sts){
    if (ee)
	foo=ee.toLowerCase().indexOf(sts);
	else{
		console.log("something is undefined");
		foo = -1;
		console.log(ee);
		console.log(sts);
	}
    if (foo>-1){
      e=ee.substr(0,foo) + '<FONT style="BACKGROUND-COLOR: yellow">'+ ee.substr(foo,sts.length)  + '</FONT>' + ee.substr(foo+sts.length,ee.length)
    }
    else {e=ee}
    return e
  }

  function signsymbol(x){
    plus =  '&plus;' //'&#8593;' '&#43' '&gt'; 
    minus = '&minus;' // &#8595;' '&#45' '&lt;';
    neutral = '=' //&#8776;'
    if (x>5){symb=plus}
    if (x>-5 && x<5){symb=neutral}
    if (x<-5){symb=minus}
    return symb
}
  
  function itemsTables(stuff,txt,txtt,ext) {
   mytable = "<div class='"+ext+"'>";
    mytable += '<'+'table style="width:100%">'
    mytable+="<"+"colgroup><"+"col span='1' style='width: 72%;'><"+"col span='1' style='width: 14%;'><"+"col span='1' style='width: 14%;'></"+"colgroup>"
    mytable+='<'+'tr><'+'th colspan="3" align="center" style="font-size:16px;">' + txtt + '</'+'th></'+'tr>'
    mytable+="<"+"tr><"+"th>"+txt+"</"+"th><"+"th align='right'>f(%)</"+"th><"+"th align='center'>&sigma;</"+"th></"+"tr>"
    //mytable+="<"+"tr><"+"th>"+txt+"</"+"th><"+"th align='right'>f(%)</"+"th><"+"th align='center'>sign.</"+"th></"+"tr>"
    stuff.forEach(function(elt) { 
      ee=auxSTS(elt[0],stuff_to_search)
      mytable+="<"+"tr><"+"td>"+ee+"</"+"td><"+"td align='right' >"+elt[1]+"</"+"td><"+"td align='center'>"+signsymbol(elt[2])+"</"+"td></"+"tr>"
    })
    mytable += '</'+'table><br/>'
    return mytable
   }

  //---------------------

  function authorsTables(stuff,txt) {
    mytable = '<'+'table style="width:100%">'
    mytable+="<"+"tr><"+"th style='font-size:16px;'>"+txt+"</"+"th></"+"tr>"
    stuff.forEach(function(elt) { 
      ee=auxSTS(elt[0],stuff_to_search)
      mytable+='<'+'tr><'+'td><span class="title"><a href="'+wwwlink(ee)+'" target="new">' + ee+ "</a></span>, <span class='nums'>"+ elt[2] +" papers / " + elt[1] + " citations</span></"+"td></"+"tr>"
    })
    //<br/><span class='abstract'>" + elt[4] + "</span>"
    //", In-degree: " + elt[6] + 
    mytable += '</'+'table><br/>'
    return mytable
  }

  //---------------------

  function articlesTables(stuff,txt, exClass) {
    ii=1;aux=true;
    mytable = '<'+'table class="articleTable" style="width:100%">'
    mytable+="<"+"tr><"+"th style='font-size:16px;'>"+txt+"</"+"th></"+"tr>"
   var count = 0;
    stuff.forEach(function(elt,ind) { 
      if (aux){
        gg=auxSTS(elt[0],stuff_to_search) 
        ee=auxSTS(elt[1],stuff_to_search) 
		
		var trow = jQuery("<tr>");
		var col1 = jQuery("<td>"); 
		col1.html('<span class="title"><strong>'+(++count)+'.<a href="' + wwwlink(elt[0]) + '" target="new">' + gg + "</a></strong> ["+elt[5]+"]</span><br><br/><span class='source'>" + ee + "<br><i>" + elt[2] + " (" + elt[3] + ")</i></span><br><br><span class='nums'>Citations:" + elt[4] + "</span>");
		
		if (myTiles != "LOGGEDOUT"){
			col1.css("width","80%");
			
			trow.append(col1);
			var col2 = jQuery("<td>").css("height",1);
			col2.append(jQuery("<div>").addClass("articleSaver").attr("onclick","saveArticle('"+exClass+"',"+ind+")"));
			
			trow.append(col2);
		
		} else{
			trow.append(col1);
		}
		
	
        mytable+=trow.prop("outerHTML");
		}
		
      ii+=1;
      if (ii === 21) {aux = false};
    })
    //<br/><span class='abstract'>" + elt[4] + "</span>"
    //", In-degree: " + elt[6] + 
    mytable += '</'+'table><br/>'
    return mytable
  }

  function wwwlink(foo) {
    foo="https://scholar.google.com/scholar?hl=com&q="+foo.replace(' ','+');
    return foo
  }
  
  
  

  // --------------------------------------------------------------------- 

  //d3.json('/wp-content/uploads/sites/2/2015/12/clusters.txt',
  console.log("load in the JSON for clusters");
  d3.json('/wp-content/themes/researchmap/data/clusters_thr2.json',
    function(data) {
		
	var matrix = [];
	var indexByName = {};
	var nodes = [];
	
	createColorKey();

    // filter the nodes to keep according to filters 
    var nodeArrayf = data.nodes.filter( function(d) { return ((keep === undefined) && (level_option === 'TOP') && (d.type === "top")) || ((d.type === "subtop") && (d.id_top === keep))  || ((keep === undefined) && (level_option === 'SUB') && (d.type === 'subtop') ); })
    maxNodeSize = Math.max.apply( null, nodeArrayf.map( function(n) {return n.size;} ) );
    var nodeArray = nodeArrayf.filter( function(d) {return (d.size >= minS*minS*maxNodeSize) }); 
    // create empty list of the nodes links
    nodeArray.forEach(function(n){n.links=[];});

    //console.log (nodeArray);

    // filter the links (keep only those with kept nodes and weight > minW)
    // and update the nodes' lists of links 
    var linkArray = [];
    data.links.forEach(function(e) { 
      var sourceNode = nodeArray.filter(function(n) { return n.name === e.source; });
      var targetNode = nodeArray.filter(function(n) { return n.name === e.target; });
      if ( (e.weight > minW)  & (sourceNode.length >0) & (targetNode.length >0) ) {
        linkArray.push({source: sourceNode[0], target: targetNode[0], weight: e.weight});
        sourceNode[0].links.push(targetNode[0].name);
        targetNode[0].links.push(sourceNode[0].name);
      }
    }); 
	
	var nodeList = nodeArrayf.filter( function(d) {return (d.size >= minS*minS*maxNodeSize) }); 
	
			nodeList.forEach(function(n){
				indexByName[n.name] = nodes.length;
				var nodeInfo = {};
				nodeInfo.name = n.name;
				nodeInfo.label = n.label;
				nodeInfo.size = n.size;
				nodes.push(n);
			})
	
			for (var i = 0; i < nodes.length;i++){
				matrix[i] = [];
			}
	
			var linksList = data.links.filter(function(n){
				return (indexByName[n.source] > -1 && indexByName[n.target] > -1);
			})
	
			linksList.forEach(function(n){
				if (n.weight > minW){
					matrix[indexByName[n.source]][indexByName[n.target]] = n.weight;
					matrix[indexByName[n.target]][indexByName[n.source]] = n.weight;
				}
				else {
					matrix[indexByName[n.source]][indexByName[n.target]] = 0;
					matrix[indexByName[n.target]][indexByName[n.source]] = 0;
				}
			})
	
			for (var i = 0; i < nodes.length;i++){
				var stretch = "["
				for (var j = 0; j < nodes.length;j++){
					if (!matrix[i][j])
						matrix[i][j] = 0;
	
					stretch +=","+matrix[i][j];
				}
				stretch += "]"
			
			}

    //console.log(linkArray)
	
	if (level_option==='TOP'){
      TOPpositions.forEach(function (p){
        var Node = nodeArray.filter(function(n) { return n.name === p[0].toString()  ; });
        if (Node.length > 0){
          Node[0].myX=p[1];
          Node[0].myY=p[2];
        }
      })
    }
    if (level_option==='SUB' || keep !== undefined){
      SUBpositions.forEach(function (p){
        var Node = nodeArray.filter(function(n) { return n.name === p[0].toString()  ; });
        if (Node.length > 0){
          Node[0].myX=p[1];
          Node[0].myY=p[2];
        }
      })
    }

    // min and max XY
    minX=Math.min.apply( null, nodeArray.map( function(n) {return n.myX;} ) );
    maxX=Math.max.apply( null, nodeArray.map( function(n) {return n.myX;} ) );
    minY=Math.min.apply( null, nodeArray.map( function(n) {return n.myY;} ) );
    maxY=Math.max.apply( null, nodeArray.map( function(n) {return n.myY;} ) );

    // scale for my position
    if (level_option==='TOP'){
      var myposx = d3.scale.linear().domain([minX,maxX]).range([0.05*WIDTH,.8*WIDTH]).clamp(true);
      var myposy = d3.scale.linear().domain([minY,maxY]).range([0.1*HEIGHT,.8*HEIGHT]).clamp(true);
    }
    if (level_option==='SUB'){
      var myposx = d3.scale.linear().domain([minX,maxX]).range([0.05*WIDTH,.85*WIDTH]).clamp(true);
      var myposy = d3.scale.linear().domain([minY,maxY]).range([0.1*HEIGHT,.85*HEIGHT]).clamp(true);
    }

    // give initial position
    nodeArray.forEach( function(n){ 
      if (layout_option === 'FIXED') { n.x=myposx(n.myX); n.y=myposy(n.myY); } 
      else {n.x=Math.floor(Math.random() * 100)*WIDTH/100; n.y=Math.floor(Math.random() * 100)*HEIGHT/100;}
      });

    // min and max sizes
    /*minNodeSize = Math.min.apply( null, nodeArray.map( function(n) {return n.size;} ) );
    maxNodeSize = Math.max.apply( null, nodeArray.map( function(n) {return n.size;} ) );
    minLinkWeight = Math.min.apply( null, linkArray.map( function(n) {return n.weight;} ) );
    maxLinkWeight = Math.max.apply( null, linkArray.map( function(n) {return n.weight;} ) );
    console.log(maxLinkWeight)*/

    maxNodeSize = GENmaxNodeSize;
    if (level_option === 'TOP') {maxLinkWeight = TOPmaxLinkWeight;}
    if (level_option === 'SUB') {maxLinkWeight = SUBmaxLinkWeight;}
    if (keep !== undefined) {maxNodeSize = SSSmaxNodeSize;}

    // A couple of scales for node radius & edge width
    var node_size = d3.scale.pow().exponent(0.5)
      .domain([0,maxNodeSize])
      .range([1,25])
      .clamp(true);
    var edge_width = d3.scale.pow().exponent(1)
      .domain( [0,maxLinkWeight] )
      .range([0.2,6])
      .clamp(true);


    // Add the node & link arrays to the layout, and start 
    // the D3.js force-directed layout
	
	function createColorKey(){
		var tops = data.nodes.filter(function(d){return (d.type == "top")});
		var col = jQuery("#colorKey").html("Color Key:<br>");
		
		for (var i = 0; i < tops.length;i++){
			var tCol = jQuery("<div>").attr("class","topicColor").attr("id","topicColor"+tops[i].group);
			col.append(tCol);
			console.log("Label:"+tops[i].label+" color:"+color(tops[i].group));
			
			var circ = '<svg class="keyCircle" height="14" width="15"><circle cx="8" cy="7" r="7" fill="'+color(tops[i].group)+'" /></svg>'
			//var kCirc = jQuery("<svg>").attr({"class":"keyCircle",height:"14",width:"14"});
			//kCirc.append(jQuery("<circle>").attr({cx:8,cy:7,r:7,fill:color(tops[i].group)}));
			var kLabel = jQuery("<span>").attr("class","keyLabel").html(tops[i].label);
			
			//tCol.append(kCirc);
			d3.select("#topicColor"+tops[i].group).html(circ);
			tCol.append(kLabel);
			
		}
	}
	

	if (graph_type === "FORCE")
		createForceLayout();
	if (graph_type === "CHORD")
		createChordLayout();
	function createForceLayout(){
		var force = d3.layout.force()
	
		if (level_option ==='TOP'){
		force  
		  .gravity(TOPforce[0])
		  .charge(TOPforce[1])
		  .distance(TOPforce[2])
		  .size( [WIDTH, HEIGHT] )
		}
	
		if (level_option ==='SUB'){
		force  
		  .gravity(SUBforce[0])
		  .charge(SUBforce[1])
		  .distance(SUBforce[2])
		  .size( [WIDTH, HEIGHT] )
		} 
	
		if (layout_option === 'FORCE'){
		force
		  .nodes(nodeArray)
		  .links(linkArray)
		  .start(); 
		}
		if (layout_option === 'FIXED'){
		force
		  .start();  
		// Add specific drag & zoom behaviours   
		svg.call( d3.behavior.drag()
		  .on("drag",dragmove) );      
		//
		}
	
		svg.call( d3.behavior.zoom()
		  .x(xScale)
		  .y(yScale)
		  .scaleExtent([1, 5])
		  .on("zoom", doZoom) 
		  ).on("dblclick.zoom", null);
		  
		  
	
	
		// ------- Create the elements of the layout (links and nodes) ------
	
		var networkGraph = svg.append('svg:g').attr('class','grpParent');
	
		// links: simple lines
		var graphLinks = networkGraph.append('svg:g').attr('class','grp gLinks')
		  .selectAll("line")
		  .data(linkArray)
		  .enter().append("line")
		  .attr('id', function(d) { return "e" + d.source.name + '-' + d.target.name; } )
		  .style('stroke-width', function(d) { return edge_width(d.weight);} )
		  .attr("class", "link");
		  
		  var doubleClicked = false;
	
		// nodes: an SVG circle
		var graphNodes = networkGraph.append('svg:g').attr('class','grp gNodes')
		  .selectAll("circle")
		  .data( nodeArray, function(d){return d.name} )
		  .enter().append("svg:circle")
		  .attr('id', function(d) { return "c" + d.name; } )
		  .attr('r', function(d) { return node_size(d.size); } )
		  .style("fill", function(d) { return color(d.group);})
		  .attr('pointer-events', 'all')  
		  .on("click", function(d) { 
		  		var self = this;
				
				setTimeout(function(){
					console.log("single click");
					if (!doubleClicked){
						console.log("single click acted");
						showClusterPanel(d);
						highlightGraphNode(d,true,this);
						 slideOutInfo(); 
					}
					else{
						console.log("single click aborted");
						
					}
				},300)
			} )
		  .on("dblclick", function(d) {
			  	doubleClicked = true;
				console.log("double click");
			  	keep=d.id_top;
				level_option='TOP';
				//layout_option='FORCE';
				updateA();
				setTimeout(function(){
					doubleClicked = false;
				},300)
			  }) 
		  .on("mouseover", function(d) { if (clusterInfoDiv.attr('class') === 'panel_off') { highlightGraphNode(d,true,this);  }} )
		  .on("mouseout", function(d) { if (clusterInfoDiv.attr('class') === 'panel_off') { highlightGraphNode(d,false,this);  }} )
		  .call(force.drag);
	
		// labels: a group with two SVG text: a title and a shadow (as background)
		var graphLabels = networkGraph.append('svg:g').attr('class','grp gLabel')
		  .selectAll("g.label")
		  .data( nodeArray, function(d){return d.name} )
		  .enter().append("svg:g")
		  .attr('id', function(d) { return "l" + d.name; } )
		  .attr('class','label');
	   
		shadows = graphLabels.append('svg:text')
		  .attr('x','-2em')
		  .attr('y','-.3em')
		  .attr('pointer-events', 'none') // they go to the circle beneath
		  .attr('id', function(d) { return "lb" + d.name; } )
		  .attr('class','nshadow')
		  .text( function(d) { return d.label; } );
	
		labels = graphLabels.append('svg:text')
		  .attr('x','-2em')
		  .attr('y','-.3em')
		  .attr('pointer-events', 'none') // they go to the circle beneath
		  .attr('id', function(d) { return "lf" + d.name; } )
		  .attr('class','nlabel')
		  .text( function(d) { return d.label; } );
		
		 
		// do search if needed
		if (stuff_to_search !== undefined){ searchStuff (stuff_to_search, nodeArray);}
	
	
		/* --------------------------------------------------------------------- */
		/* ----------------- HIGHLIGHT & INFO PANE----------------------------- */
		/* --------------------------------------------------------------------- */ 
		
		// -----------------------------------
		function highlightGraphNode( node, on )
		{ 
		  /* Select/unselect a node in the network graph.
		   Parameters are: 
		   - node: data for the node to be changed,  
		   - on: true/false to show/hide the node */
		  
		  // If we are to activate a cluster, and there's already one active, first switch that one off  
		  if( on && activeCluster !== undefined ) { 
	   highlightGraphNode( activeCluster, false );
		  }
	
		  // locate the SVG nodes: circle & label group
		  circle = d3.select( '#c' + node.name );
		  label  = d3.select( '#l' + node.name );
	
		  // activate/deactivate the node itself
		  circle
	   .classed( 'main', on );
		  label
	   .classed( 'on', on || currentZoom >= SHOW_THRESHOLD );
		  label.selectAll('text')
	   .classed( 'main', on );
	
		  // activate all siblings
		  Object(node.links).forEach( function(nm) {
	   //.. circle      
	   d3.select("#c"+nm).classed( 'sibling', on );
	   //.. labels
	   label = d3.select('#l'+nm);
	   label.classed( 'on', on || currentZoom >= SHOW_THRESHOLD );
	   label.selectAll('text.nlabel')
		 .classed( 'sibling', on );
	   label.selectAll('text.nshadow')  
		 .classed( 'nshadowB', on );
	   //.. links  
	   d3.select("#e"+node.name+'-'+nm).classed('linking', on)
	   d3.select("#e"+nm+'-'+node.name).classed('linking', on)
		   } );
	
	
		  // set the value for the current active cluster
		  activeCluster = on ? node: undefined;
		}
	
		//----------------------------------
		// Show the cluster details panel for a given node   
		function showClusterPanel( node ) {
		  // Fill it and display the panel
		  selectedNode = node;
		  clusterInfoDiv  
	   .html( getclusterInfo(node,nodeArray) )
	   .attr("class","panel_on");
	   
	
	   
	   
	   
	   jQuery(".learnMoreButton").click();
	   
		}
	
	   /* --------------------------------------------------------------------- */
	   /* ----------------- SEARCH -------------------------------------------- */
	   /* --------------------------------------------------------------------- */
	   // search the typed characters in most cited/used authors and keywords of the nodes 
		function searchStuff (stuff_to_search, nodeArray){
			
		  function mycheck(stuff, sts, which){  
			if (which === undefined) {which=0}
			res=false;
			stuff.forEach(function (elt){ 
			  if ( (elt[which].toLowerCase().indexOf(sts) > -1)  ){res=true} 
			})
			return res;
		  }
	
		  // look for stuff 
		  var foundNodesA = nodeArray.filter( function(n) { return (mycheck(n.stuffK, stuff_to_search) || mycheck(n.stuffTK, stuff_to_search) || mycheck(n.stuffJ, stuff_to_search) || mycheck(n.stuffS, stuff_to_search) || mycheck(n.MCP, stuff_to_search) || mycheck(n.MCP, stuff_to_search, 1) || mycheck(n.MCAU, stuff_to_search)  || mycheck(n.stuffR, stuff_to_search) || mycheck(n.stuffR, stuff_to_search) || (n.label.toLowerCase().indexOf(stuff_to_search) > -1) || (n.descr.toLowerCase().indexOf(stuff_to_search) > -1)   ) })
		 
	//      
		  foundNodes=[];
		  foundNodesA.forEach( function(n){ 
		
		  foundNodes.push(n.name) })
	
		  // how much  hits did we get?
		  var mylength = foundNodes.length;
	
		  // at what level are we?
		  console.log("level "+level_option);
		  console.log("keep is "+keep);
		  if ((level_option === 'TOP') && (keep ===undefined)) {truc=' topic'}
		  else {truc=' subtopic'}
		  function suff(L){str=''; if(L>1){str='s';} return str}
	
		  
		  // where was the stuff found?
		  if (mylength === 0){
			searchRes = '<br/><i>No results found at this level.</i>'
		   }
		  else {
			searchRes = '<br/><i>Found in ' + mylength + truc + suff(mylength) + '!<br/>'
			
			foundNodes.forEach(function(nm){
			  //searchRes += '<span >' + n.label + '</span><br/>'
			  d3.select( '#c' + nm ).classed( 'found', true )
			})
		  }
		  // display searRes
		  d3.select('#SearchRes').html(searchRes)
	
		}
	
	
	   /* --------------------------------------------------------------------- */
	   /* ----------------- REPOSITION ---------------------------------------- */
	   /* --------------------------------------------------------------------- */
		/* Move all graph elements to its new positions. Triggered:
		   - on node repositioning (as result of a force-directed iteration)
		   - on translations (user is panning)
		   - on zoom changes (user is zooming)
		   - on explicit node highlight (user clicks in a cluster panel link)
		   Set also the values keeping track of current offset & zoom values */
		
	
		function repositionGraph( off, z, mode ) {
		  
		 if( off !== undefined && (off.x != currentOffset.x || off.y != currentOffset.y ) ) {
        g = d3.select('g.grpParent')
          g.attr("transform", function(d) { return "translate("+off.x+","+off.y+")" } );
          currentOffset.x = off.x;
          currentOffset.y = off.y;
      }
 
      // zoom: get new value of zoom
      if( z === undefined ) {
        if( mode != 'tick' )
          return;   // no zoom, no tick, we don't need to go further
        z = currentZoom;
      }
      else
        currentZoom = z;
        // prep matrice of transform
        za=z;zb=0;zc=0;
        zd=0;ze=z;zf=0;
 
   
        // do transformation
		
        graphLinks
          .attr("x1", function(d) { return za*(d.source.x)+zb*(d.source.y)+zc; })
          .attr("y1", function(d) { return zd*(d.source.x)+ze*(d.source.y)+zf; })
          .attr("x2", function(d) { return za*(d.target.x)+zb*(d.target.y)+zc; })
          .attr("y2", function(d) { return zd*(d.target.x)+ze*(d.target.y)+zf; });
        graphNodes.attr("transform", function(d) { return "translate("+(za*d.x+zb*d.y+zc)+","+(zd*d.x+ze*d.y+zf)+")" } );
        graphLabels.attr("transform", function(d) { return "translate("+(za*d.x+zb*d.y+zc)+","+(zd*d.x+ze*d.y+zf)+")" } );
			   
		}
	   /* --------------------------------------------------------------------- */
	   /* -----------------DRAG and ZOOM -------------------------------------- */
	   /* --------------------------------------------------------------------- */
	
	
	   // ------------
	   // Perform drag
		 
		function dragmove(d) {
		  offset = { x : currentOffset.x + d3.event.dx,
		   y : currentOffset.y + d3.event.dy };
		  repositionGraph( offset, undefined, 'drag' );
		}
	   
	
	   // ---------------------------------------------------------------------
	   // semantic zoom: nodes do not change size, but get spread out or stretched together as zoom changes)
		 
		function doZoom( increment ) {
			console.log("zooming increment "+increment); 
		  newZoom = increment === undefined ? d3.event.scale 
				   : zoomScale(currentZoom+increment);
		  if( currentZoom == newZoom )
		return;  // no zoom change
		  // See if we cross the 'show' threshold in either direction
		  if( currentZoom<SHOW_THRESHOLD && newZoom>=SHOW_THRESHOLD )
		svg.selectAll("g.label").classed('on',true);
		  else if( currentZoom>=SHOW_THRESHOLD && newZoom<SHOW_THRESHOLD )
		svg.selectAll("g.label").classed('on',false);
	
		  // See what is the current graph window size
		  s = getViewportSize();
		 
		  width  = s.w<WIDTH  ? s.w : WIDTH;
		  height = s.h<HEIGHT ? s.h : HEIGHT;
	
		  // Compute the new offset, so that the graph center does not move
		  zoomRatio = newZoom/currentZoom;
		  newOffset = { x : currentOffset.x*zoomRatio + width/2*(1-zoomRatio),
			  y : currentOffset.y*zoomRatio + height/2*(1-zoomRatio) };  
	
		  // Reposition the graph
		  repositionGraph( newOffset, newZoom, "zoom" );
		}
	
	
		// ------------------------------------------
		// process events from the force-directed graph 
		force.on("tick", function() {
		  repositionGraph(undefined,undefined,'tick');
		});
	}
	//START CHORD DIAGRAM FUNCTIONS
	function createChordLayout(){
		
		var outerRadius = 800 / 2;
		var baseOuterRadius = 400;
		var innerRadius = outerRadius - 100;
	
		//var fill = d3.scale.category20c();
		
		var chord = d3.layout.chord()
			.padding(.04)
			.sortSubgroups(d3.descending)
			.sortChords(d3.descending);
		
		var arc = d3.svg.arc()
			.innerRadius(innerRadius)
			.outerRadius(innerRadius + 20);
		
		
		
		
		/*var svg = d3.select("#chordArea").append("svg")
		.attr("width", outerRadius * 2.5)
		.attr("height", outerRadius * 2.5)
		.attr("style","overflow:visible;")
	  .append("g")
		.attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");*/
		
		 chord.matrix(matrix);
	
			   var g = svg2.selectAll(".group")
				  .data(chord.groups)
				.enter().append("g")
				  .attr("class", "group")
				  .attr("transform", "translate("+WIDTH/2+","+(outerRadius+150)+")");
				  
				  svg2.call( d3.behavior.drag()
		  			.on("drag",dragmove) ); 
					
				svg2.call( d3.behavior.zoom()
				  .x(xScale)
				  .y(yScale)
				  .scaleExtent([0.5, 6])
				  .on("zoom", doZoom) 
				  ).on("dblclick.zoom", null);
					
					
			var doubleClick = false;
			  var graphPaths = g.append("path")
			  	.classed("arc",true)
				  .style("fill", function(d) { return color(nodes[d.index].group); })
				  .style("stroke", function(d) { return color(nodes[d.index].group); })
				  .attr("d", arc)
				  .on("dblclick",function(d){
					  doubleClick = true;
					  keep=nodes[d.index].id_top;level_option='TOP';layout_option='FORCE';updateA();
					  setTimeout(function(){
						  doubleClick = false;
					  },310)
					  })
				  .on("click", function(d){
					 setTimeout(function(){
					 	if (!doubleClick){
							slideOutInfo();
					 	 	showClusterPanel(nodes[d.index]);
						}
					 },300)
					  })
				  .on("mouseover", fadeOut)
    				.on("mouseout", fadeIn);
	
			   var graphLabels = g.append("text")
				  .each(function(d) { d.angle = (d.startAngle + d.endAngle) / 2; })
				  .attr("class","chordLabel")
				  .attr("dy", ".3em")
				  .attr("transform", function(d) {
					return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
						+ "translate(" + (innerRadius + 26) + ")"
						+ (d.angle > Math.PI ? "rotate(180)" : "");
				  })
				  .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
				  .text(function(d) { return nodes[d.index].label });
	
			/*  var graphNodes = networkGraph.append('svg:g').attr('class','grp gNodes')
			  .selectAll("circle")
			  .data( nodeArray, function(d){return d.name} )
			  .enter().append("svg:circle")
			  .attr('id', function(d) { return "c" + d.name; } )
			  .attr('r', function(d) { return node_size(d.size); } )
			  .style("fill", function(d) { return color(d.group);})
			  .attr('pointer-events', 'all')  
			  .on("click", function(d) { showClusterPanel(d);highlightGraphNode(d,true,this); } )
			  .on("dblclick", function(d) {keep=d.id_top;level_option='TOP';layout_option='FORCE';updateA();}) 
			  .on("mouseover", function(d) { if (clusterInfoDiv.attr('class') === 'panel_off') { highlightGraphNode(d,true,this);  }} )
			  .on("mouseout", function(d) { if (clusterInfoDiv.attr('class') === 'panel_off') { highlightGraphNode(d,false,this);  }} )
			  .call(force.drag);*/
			  
			  svg2.selectAll(".chord")
				  .data(chord.chords)
				.enter().append("path")
				  .attr("class", "chord")
				  .style("stroke", function(d) { return d3.rgb(color(nodes[d.source.index].group)).darker(); })
				  .style("fill", function(d) { return color(nodes[d.source.index].group); })
				  .attr("d", d3.svg.chord().radius(innerRadius))
				  .attr("transform", "translate("+WIDTH/2+","+(outerRadius+150)+")");
				  
				  currentOffset.x = WIDTH/2;
				  currentOffset.y = outerRadius+150;
				  
					if (stuff_to_search !== undefined){ searchStuff (stuff_to_search, nodes);}
	
	
		/* --------------------------------------------------------------------- */
		/* ----------------- HIGHLIGHT & INFO PANE----------------------------- */
		/* --------------------------------------------------------------------- */ 
		
		// -----------------------------------
	
	
	   /* --------------------------------------------------------------------- */
	   /* ----------------- SEARCH -------------------------------------------- */
	   /* --------------------------------------------------------------------- */
	   // search the typed characters in most cited/used authors and keywords of the nodes 
		function searchStuff (stuff_to_search, nodeArray){
			
		  function mycheck(stuff, sts, which){  
			if (which === undefined) {which=0}
			res=false;
			stuff.forEach(function (elt){ 
			  if ( (elt[which].toLowerCase().indexOf(sts) > -1)  ){res=true} 
			})
			return res;
		  }
	
		  // look for stuff 
		  var foundNodesA = nodeArray.filter( function(n) { return (mycheck(n.stuffK, stuff_to_search) || mycheck(n.stuffTK, stuff_to_search) || mycheck(n.stuffJ, stuff_to_search) || mycheck(n.stuffS, stuff_to_search) || mycheck(n.MCP, stuff_to_search) || mycheck(n.MCP, stuff_to_search, 1) || mycheck(n.MCAU, stuff_to_search)  || mycheck(n.stuffR, stuff_to_search) || mycheck(n.stuffR, stuff_to_search) || (n.label.toLowerCase().indexOf(stuff_to_search) > -1) || (n.descr.toLowerCase().indexOf(stuff_to_search) > -1)   ) })
		 
	//      
		  foundNodes=[];
		  foundNodesA.forEach( function(n){ 
		  
		  foundNodes.push(n.name) })
	
		  // how much  hits did we get?
		  var mylength = foundNodes.length;
	
		  // at what level are we?
		  if ((level_option === 'TOP') && (keep ===undefined)) {truc=' topic'}
		  else {truc=' subtopic'}
		  function suff(L){str=''; if(L>1){str='s';} return str}
	
		  
		  // where was the stuff found?
		  if (mylength === 0){
			searchRes = '<br/><i>No results found at this level.</i>'
		   }
		  else {
			
			searchRes = '<br/><i>Found in ' + mylength + truc + suff(mylength) + '!<br/>'
			var foundIndexList = [];
			foundNodes.forEach(function(nm){
			
				
				for (var i = 0; i < nodes.length;i++){
					if(nodes[i].name == nm){
						foundIndexList.push(i);
						break;
					}
				}
			  //searchRes += '<span >' + n.label + '</span><br/>'
			 // d3.select( '#c' + nm ).classed( 'found', true )
			 
	
			 
			})
			
		
			
			for (var i = 0; i < foundIndexList.length;i++){
				svg2.selectAll(".group")
							.filter(function(d) { return d.index == foundIndexList[i]; })
							.selectAll("path").classed( 'found', true )
			}
		  }
		  // display searRes
		  d3.select('#SearchRes').html(searchRes)
	
		}
				  
				  function fade(opacity) {
					  
					  return function(g, i) {
						svg2.selectAll(".chord path")
							.filter(function(d) { return d.source.index != i && d.target.index != i; })
						  .transition()
							.style("opacity", opacity);
					  };
					}
					
					function fadeOut(d) {
				
					
						svg2.selectAll(".chord")
							.filter(function(e) { return e.source.index != d.index  && e.target.index != d.index; })
						  .transition()
							.style("opacity", 0.1);
					}
					
					function fadeIn(d) {
					 
					 
						svg2.selectAll(".chord")
							.filter(function(e) { return e.source.index != d.index  && e.target.index != d.index; })
						  .transition()
							.style("opacity", 1);
					 
					}
					
					
					function showClusterPanel( node ) {
					  // Fill it and display the panel
					  
					  selectedNode = node;
					
					  clusterInfoDiv  
				   .html( getclusterInfo(node,nodeArray) )
				   .attr("class","panel_on");
				   
				  
				   
				   jQuery(".learnMoreButton").click();
					}
					
					function dragmove(d) {
					  offset = { x : currentOffset.x + d3.event.dx,
					   y : currentOffset.y + d3.event.dy };
					  repositionGraph( offset, undefined, 'drag' );
					}
					function doZoom( increment ) {
						console.log("chord zooming increment "+increment);
						
					  newZoom = increment === undefined ? d3.event.scale 
							   : zoomScale(currentZoom+increment);
							   
					console.log("new zoom " + newZoom);
					  if( currentZoom == newZoom )
					return;  // no zoom change
					  // See if we cross the 'show' threshold in either direction
					
				
					  // See what is the current graph window size
					  s = getViewportSize();
				
					  width  = s.w<WIDTH  ? s.w : WIDTH;
					  height = s.h<HEIGHT ? s.h : HEIGHT;
				
					  // Compute the new offset, so that the graph center does not move
					  zoomRatio = newZoom/currentZoom;
					  newOffset = { x : currentOffset.x*zoomRatio + width/2*(1-zoomRatio),
						  y : currentOffset.y*zoomRatio + height/2*(1-zoomRatio) };  
				
					  // Reposition the graph
					  repositionGraph( newOffset, newZoom, "zoom" );
					}
					
						function repositionGraph( off, z, mode ) {
							
							
						  
						  // do we want to do a transition?
						  var doTr = (mode == 'move');
					
						  // drag: translate to new offset
						  if( off !== undefined &&
						 (off.x != currentOffset.x || off.y != currentOffset.y ) ) {
					   g = d3.selectAll('.group')
					   if( doTr ){
						 g = g.transition().duration(500);
					   }
					   g.attr("transform", function(d) { return "translate("+
									 off.x+","+off.y+") scale("+currentZoom+")" } );
					 c = d3.selectAll('.chord')
					   if( doTr ){
						 c = c.transition().duration(500);
					   }
					   c.attr("transform", function(d) { return "translate("+
									 off.x+","+off.y+") scale("+currentZoom+")" } );
					   currentOffset.x = off.x;
					   currentOffset.y = off.y;
						  }
					
						  // zoom: get new value of zoom
						  if( z === undefined ) {
					   if( mode != 'tick' )
						 return;   // no zoom, no tick, we don't need to go further
					   z = currentZoom;
						  }
						  else
					   currentZoom = z;
					  svg2.selectAll(".chord").attr("transform"," translate("+currentOffset.x+","+currentOffset.y+") scale("+currentZoom+")");
					  svg2.selectAll(".group").attr("transform"," translate("+currentOffset.x+","+currentOffset.y+") scale("+currentZoom+")");
					   
					   
					
						
						 /* // move edges
						  var allChords = d3.selectAll(".chord");
						   c = doTr ? allChords.transition().duration(500) : allChords;
						  
						  c
					   .attr("x1", function(d) { return z*(d.source.x); })
							.attr("y1", function(d) { return z*(d.source.y); })
							.attr("x2", function(d) { return z*(d.target.x); })
							.attr("y2", function(d) { return z*(d.target.y); });
					
						  // move nodes
						 // n = doTr ? graphNodes.transition().duration(500) : graphNodes;
						 var allGroups = d3.selectAll(".group");
						  g = doTr ? allGroups.transition().duration(500) : allGroups;
						  g
					   .attr("transform", function(d) { return "translate("
									+z*d.x+","+z*d.y+")" } );
						   move labels*/
						
						}
				  	
	}

  });

}