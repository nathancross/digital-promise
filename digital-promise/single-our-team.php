<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">
            
            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>
                            <div class="Grid Grid--gutters Grid--full large-Grid--fit">
                            
                                   
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <div class="Grid-cell u-large-1of4 u-med-1of4 u-small-full" style="margin-top:40px; padding:0 30px;"><?php the_post_thumbnail( 'full' ); ?></div>
                                    <?php endif; ?>
                                    
                                    <div class="Grid-cell u-large-3of4 u-med-3of4 u-small-full">
                                        <h1 class="page-header"><?php the_title() ;?></h1>
                                        <?php the_content(); ?>
                                    </div>

                                    </div><!-- /end grid container -->
                            

                        </div>
                    

                     <?php  endif; ?>

                <?php endwhile; ?>
                

                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>

   </div>

</div>
<!-- end content container -->

<?php get_footer(); ?>

