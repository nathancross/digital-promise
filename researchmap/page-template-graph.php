<?php
/*
Template Name: Data Display Graph
*/
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


	
	<div id="content">
	
		<div id="inner-content" class="row">
        
        <div id="itemSaver" class="popup">
        	<div id="saverClose"></div>
            <div id="saverInfoArea">
            <h2 id="saverNodeName">Lorem Ipsum</h2><br>
            <p id="saverNodeInfo">Fortunate senex ergo tua rura manebat, et tibi magna satis quamvis lapis omnia nudas</p>
            </div>
            <div id="saverUserArea">
            My Comments:
                <textarea maxlength="300" id="saverComments"></textarea>
                <select id="saverGroupSelector"></select><span style="float:right;">Save to:</span>
                <div style="clear:both;"></div>
                <div id="saverSaveButton">Save</div>
                <div style="clear:both;"></div>
            </div>
        
        </div>
        
        <?php get_sidebar("sidebarFilter"); ?>
	
		    <main id="main" class="centerContent" role="main">
				
				 <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="page-header waypoint"><?php the_title() ;?></h2>
            <?php the_content(); ?>
            <?php wp_link_pages(); ?>
            <?php comments_template(); ?>

        <?php endwhile; ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>					
			    					
			</main> <!-- end #main -->

		<?php get_sidebar("sidebarData"); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>