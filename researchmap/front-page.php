<?php
/*
Template Name: Hero Homepage
*/
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


<div class="row dmbs-content">
        <div class="homepage-feature">
            <div class="subsite-name">Research Map</div>
        <?php 

        $args = array( 'post_type' => 'homepage-hero', 'posts_per_page' => 1 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

                    <div <?php post_class(); ?>>

                        <?php get_template_part( 'content', 'page' ); ?>

                            <?php if ( has_post_thumbnail() ) : ?>
                                               <?php the_post_thumbnail("full"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>
                        
                        </div>
            <h1 class="hero-title">Explore</h1>
            <a class="btn btn-gold btn-action force" href="/views/network/">Network View</a>
            <a class="btn btn-gold btn-action chord" href="/views/chord/">Chord View</a>
        
		<?php endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div>
</div>
<!-- / end homepage hero/feature area -->


<!--   CONTENT AFTER HERO HERE -->



<div class="row dmbs-content">
   
   	<div class="col-md-9">
        <?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
            <?php get_404_template(); ?>
        <?php endif; ?>
   	</div>
   
  	<div class="col-md-3 promo-image">
		<img style="margin-top:15px;" src="/wp-content/uploads/sites/2/2016/05/most-viewed-topics.jpg">
	</div>
   
</div>

<div class="row dmbs-content">
   
   	<div class="col-md-9">
		<h3>Research News</h3>
        
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
        <?php
        
        // change to primary site
        switch_to_blog(1);
        
        $args = array(
        	'post_type'      => 'post',
    		'posts_per_page' => 3,
    		'order'          => 'DESC',
    		'orderby'        => 'date',
        	'post_status' 	 => 'publish',
   	        'cat'  => 39, // 39 = our-research on the primary site
        );
        
        
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                       
            <div class="Grid-cell u-large-1of3 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                           
                           <?php if ( has_post_thumbnail() ) : ?>
                               <?php the_post_thumbnail("medium"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>

							<p class="byline vcard" style="margin-bottom:1em;">
								<time class="updated" datetime="<?php echo get_the_date('Y-m-d'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
							</p>

                            <h3 class="newsontopic">
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h3>

                            
                            <!--  the_excerpt(); -->

                       </div>
                    </div>

            </div>

		<?php endwhile; // end of the loop. ?>

        <?php
        wp_reset_postdata(); 
        
        //switch back to this site
        restore_current_blog();
        ?>
        </div><!-- /end grid container -->

        
   	</div>
   
  	<div class="col-md-3">
		<h3>Most Viewed Topics</h3>
		
		<?php 
		if (function_exists('wpp_get_mostpopular')) {
			//////////////////////////////////////////////////////////////////////////
			// NOTE: Categories are not native to pages, so a wpp_get_mostpopular
			// will not normally take post_type=page AND cat=6 at the same time.
			// In order to make categories work with pages in this plugin, you 
			// must comment out some code in the plugin and this WILL BE OVERWRITTEN
			// if the plugin is updated. If this page begins showing all pages (and 
			// not just Topic Pages) in the future, check the plugin 'wordpress-popular-posts
			// and comment out the following:
			//
			//		// if we're getting just pages, why join the categories table?
			//		if ( 'page' == strtolower($instance['post_type']) ) {
			//			//$join_cats = false; // THIS IS THE LINE TO FIND AND COMMENT OUT
			//			$where .= " AND p.post_type = '{$instance['post_type']}'";
			//
			//		}		
		
			// cat 6 is "Topic Page" 
			wpp_get_mostpopular('cat="6"&limit=5&post_type="page"&stats_views=0'); 

			//////////////////////////////////////////////////////////////////////////
		}
		?>
		
	    <a href="/topics/" style="float:right;" class="btn btn-blue-light btn-action">All Topics</a>

		
	</div>
   
</div>




</div>
<!-- end content container -->

<?php get_footer(); ?>
