<?php
/*
Template Name: Search Page
*/
get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-9 dmbs-main">

        <h2 class='page-header'>Search this site</h2>
        
        
        <?php
        global $query_string;

        $query_args = explode("&", $query_string);
        $search_query = array();

        if( strlen($query_string) > 0 ) {
            foreach($query_args as $key => $string) {
                $query_split = explode("=", $string);
                $search_query[$query_split[0]] = urldecode($query_split[1]);
            } // foreach
        } //if

        $search = new WP_Query($search_query);
        ?>
        
        
        <?php get_search_form(); ?>
        
        
        <?php
        global $wp_query;
        $total_results = $wp_query->found_posts;
        ?>
        

    </div>

    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>
