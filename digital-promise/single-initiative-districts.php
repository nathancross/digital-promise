<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<?php
//******************************************************* //
// ***************** B E G I N  K E Y ******************* //
$thisGMAPIkey = 'AIzaSyCuO2mbqOelxZV1koLmueKsjNT2jNOVGDE';
// ******************* E N D   K E Y ********************* //
// ******************************************************* //
?>

<!-- start content container -->
<div class="row dmbs-content">
    
    <div class="col-md-12 breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
        <?php if(function_exists('bcn_display'))
        {
            bcn_display();
        }?>
    </div>

    <div class="col-md-12 dmbs-main">
        

        <?php

            //if this was a search we display a page header with the results count. If there were no results we display the search form.
            if (is_search()) :

                 $total_results = $wp_query->found_posts;

                 echo "<h2 class='page-header'>" . sprintf( __('%s Search Results for "%s"','devdmbootstrap3'),  $total_results, get_search_query() ) . "</h2>";

                 if ($total_results == 0) :
                     get_search_form(true);
                 endif;

            endif;

        ?>

            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>
                            <h2 class="page-header"><?php the_title() ;?></h2>
                            
                            <?php the_content(); ?>
                            
                            <div class="maptopbuttons" style="text-align:right; padding:0 15px;"><h2><a style="float:none;" class="btn btn-blue-light btn-action" href="" onclick="toggleStates(); return false;" id="maptopbuttons_bystate">BY STATE</a><a style="margin-left:40px; float:none;" class="btn btn-blue-light btn-action" href="" onclick="toggleFilters(); return false;" id="maptopbuttons_filter">FILTER</a></h2></div>
                            
                            <form action="">
                            
                            <div class="mapstatefilter" id="mapstatefilter" align="right">
								<select name="mapstatefilterselect" id="mapstatefilterselect" onchange="filterStates()">
									<option value="All Locations">All Locations</option>
									<option value="Alabama">Alabama</option>
									<option value="Alaska">Alaska</option>
									<option value="Arizona">Arizona</option>
									<option value="Arkansas">Arkansas</option>
									<option value="California">California</option>
									<option value="Colorado">Colorado</option>
									<option value="Connecticut">Connecticut</option>
									<option value="Delaware">Delaware</option>
									<option value="District of Columbia">District of Columbia</option>
									<option value="Florida">Florida</option>
									<option value="Georgia">Georgia</option>
									<option value="Hawaii">Hawaii</option>
									<option value="Idaho">Idaho</option>
									<option value="Illinois">Illinois</option>
									<option value="Indiana">Indiana</option>
									<option value="Iowa">Iowa</option>
									<option value="Kansas">Kansas</option>
									<option value="Kentucky">Kentucky</option>
									<option value="Louisiana">Louisiana</option>
									<option value="Maine">Maine</option>
									<option value="Maryland">Maryland</option>
									<option value="Massachusetts">Massachusetts</option>
									<option value="Michigan">Michigan</option>
									<option value="Minnesota">Minnesota</option>
									<option value="Mississippi">Mississippi</option>
									<option value="Missouri">Missouri</option>
									<option value="Montana">Montana</option>
									<option value="Nebraska">Nebraska</option>
									<option value="Nevada">Nevada</option>
									<option value="New Hampshire">New Hampshire</option>
									<option value="New Jersey">New Jersey</option>
									<option value="New Mexico">New Mexico</option>
									<option value="New York">New York</option>
									<option value="North Carolina">North Carolina</option>
									<option value="North Dakota">North Dakota</option>
									<option value="Ohio">Ohio</option>
									<option value="Oklahoma">Oklahoma</option>
									<option value="Oregon">Oregon</option>
									<option value="Pennsylvania">Pennsylvania</option>
									<option value="Rhode Island">Rhode Island</option>
									<option value="South Carolina">South Carolina</option>
									<option value="South Dakota">South Dakota</option>
									<option value="Tennessee">Tennessee</option>
									<option value="Texas">Texas</option>
									<option value="Utah">Utah</option>
									<option value="Vermont">Vermont</option>
									<option value="Virginia">Virginia</option>
									<option value="Washington">Washington</option>
									<option value="West Virginia">West Virginia</option>
									<option value="Wisconsin">Wisconsin</option>
									<option value="Wyoming">Wyoming</option>
									<option value="Other Locations">Other Locations</option>
								</select>
                            </div>

                            <div class="mapactivefilters" id="mapactivefilters">
                            
                            	<p>Select options to filter results. Note that if a district meets any of the criteria you choose, it will be shown.</p>
                            	
                            	<div class="mapactivefilterscolumns" style="-webkit-column-count: 4; -moz-column-count: 4; column-count: 4;">
                            		<div class="mapactivefilterscolumn_1" style="display: inline-block;">
                            		<p><strong>Geographic Location</strong><br/>
                            		<input type="checkbox" name="district_locale" value="Rural" onclick="filterMarkers()">Rural<br/>
                            		<input type="checkbox" name="district_locale" value="Suburban" onclick="filterMarkers()">Suburban<br/>
                            		<input type="checkbox" name="district_locale" value="Urban" onclick="filterMarkers()">Urban
                            		</p>
                            		                            	
                            		<p><strong>% Free or Reduced Lunch</strong><br/>
                            		<input type="checkbox" name="percent_free_reduced_lunch" value="0-25" onclick="filterMarkers()">Under 25%<br/>
                            		<input type="checkbox" name="percent_free_reduced_lunch" value="26-50" onclick="filterMarkers()">26%-50%<br/>
                            		<input type="checkbox" name="percent_free_reduced_lunch" value="51-75" onclick="filterMarkers()">51%-75%<br/>
                            		<input type="checkbox" name="percent_free_reduced_lunch" value="76-99" onclick="filterMarkers()">76%-99%<br/>
                            		<input type="checkbox" name="percent_free_reduced_lunch" value="100-100" onclick="filterMarkers()">100%
                            		</p>
                            		</div>
                            		                            	
                            		<div class="mapactivefilterscolumn_2" style="display: inline-block;">
                            		<p><strong>% with High Speed Internet</strong><br/>
                            		<input type="checkbox" name="percent_broadband" value="0-25" onclick="filterMarkers()">Under 25%<br/>
                            		<input type="checkbox" name="percent_broadband" value="26-50" onclick="filterMarkers()">26%-50%<br/>
                            		<input type="checkbox" name="percent_broadband" value="51-75" onclick="filterMarkers()">51%-75%<br/>
                            		<input type="checkbox" name="percent_broadband" value="76-99" onclick="filterMarkers()">76%-99%<br/>
                            		<input type="checkbox" name="percent_broadband" value="100-100" onclick="filterMarkers()">100%
                            		</p>

                            		<p><strong>% Students with Personal School-provided Device (1:1)</strong><br/>
                            		<input type="checkbox" name="percent_1_to_1" value="0-25" onclick="filterMarkers()">Under 25%<br/>
                            		<input type="checkbox" name="percent_1_to_1" value="26-50" onclick="filterMarkers()">26%-50%<br/>
                            		<input type="checkbox" name="percent_1_to_1" value="51-75" onclick="filterMarkers()">51%-75%<br/>
                            		<input type="checkbox" name="percent_1_to_1" value="76-99" onclick="filterMarkers()">76%-99%<br/>
                            		<input type="checkbox" name="percent_1_to_1" value="100-100" onclick="filterMarkers()">100%
                            		</p>
                            		</div>
                        		
                            		<div class="mapactivefilterscolumn_3" style="display: inline-block;">
                            		<p><strong>Topics</strong><br/>
                            		District Work<br/>
                            		<div id="district_works_list"></div>
                            		</p>
									</div>
									
									<div class="mapactivefilterscolumn_4" style="display: inline-block;">
                            		<p><br/>
                            		Digital Promise Initiatives<br/>
                            		<div id="district_dp_initiatives_list"></div>
                            		</p>
                            		</div>

                            	</div>

                           		<div class="resetmapbutton" style="text-align:right; padding:0 15px;"><h2><a style="float:none;" class="btn btn-blue-light btn-action" href="" onclick="resetAllMarkers(true); return false;">RESET FILTERS</a></h2></div>

                            </div>
                            
							</form>
                            
<!-- ******************************************************* -->
<!-- ***************** B E G I N   M A P ******************* -->
<div id="map_wrapper">                            
    <div id="map_canvas_1" style="background-color: #1c4051;"></div>
    <div id="map_canvas_2" style="background-color: #1c4051;"></div>
    <div id="map_canvas_3" style="background-color: #1c4051;"></div>
    
    <div class="panel slide-right">
        <div class="panel-content" id="panel-content">Select a district by clicking any pin on the map.</div>
    </div>
    <div class="panel-toggle-tab" id="panel-toggle-tab"><a class="vertical-text"></a></div>
</div>

<script>

var id;
var markers = [];
var districtsArray = new Array();
var districtWorksListArray = new Array();
var dpInitiativesListArray = new Array();

var panelIsOpen = false;

var filtersOn = false;
document.getElementById('mapactivefilters').style.display = 'none';
document.getElementById('mapstatefilter').style.display = 'none';

<?php
$args = array(
	'echo'             => 0,
	'child_of'		   => 76,
);
$districtWorksList = get_categories($args);
//print_r ( $districtWorksList );
foreach ($districtWorksList as $v) {
	echo "districtWorksListArray.push('" . $v->cat_name . "');";
}

$args = array(
	'echo'             => 0,
	'child_of'		   => 77,
);
$dpInitiativesList = get_categories($args);
//print_r ( $dpInitiativesList );
foreach ($dpInitiativesList as $v) {
	echo "dpInitiativesListArray.push('" . $v->cat_name . "');";
}

?>

//document.write(dpInitiativesListArray.length + "<br/>");

var stylesArray = [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "lightness": "20"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#54695e"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#51bea6"
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#51bea6"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#071014"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#939393"
            }
        ]
    }
];

function initMap() {
	USA = new google.maps.LatLng(39.899202, -95.229492);
	USAzoom = 4;
	ALASKA = new google.maps.LatLng(64.0000, -150.0000);
	ALASKAzoom = 3;
	GERMANY = new google.maps.LatLng(51.394922, 10.349121);
	GERMANYzoom = 4;
	
	// bounds of the desired area: LOWER 48
	allowedBounds_lower48 = new google.maps.LatLngBounds(
		 new google.maps.LatLng(23.885838, -129.902344), 
		 new google.maps.LatLng(49.152970, -61.699219)
	);
	// bounds of the desired area: ALASKA
	allowedBounds_alaska = new google.maps.LatLngBounds(
		 new google.maps.LatLng(51.289406, -171.298828), 
		 new google.maps.LatLng(71.244356, -129.111328)
	);
	// bounds of the desired area: GERMANY
	allowedBounds_germany = new google.maps.LatLngBounds(
		 new google.maps.LatLng(46.800059, 4.174805), 
		 new google.maps.LatLng(55.229023, 15.952148)
	);

	map_lower48 = new google.maps.Map(document.getElementById('map_canvas_1'), {
		center: USA,
		zoom: USAzoom,
		minZoom: USAzoom,
		maxZoom: 8,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: true,
		zoomControlOptions: {
       		position: google.maps.ControlPosition.LEFT_BOTTOM
   		},
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false,
		styles: stylesArray,	
	});
	var lastValidCenter_lower48 = map_lower48.getCenter();
	google.maps.event.addListener(map_lower48, 'center_changed', function() {
    	if (allowedBounds_lower48.contains(map_lower48.getCenter())) {
       	 	// still within valid bounds, so save the last valid position
    	    lastValidCenter_lower48 = map_lower48.getCenter();
    	    return; 
    	}
    	// not valid anymore => return to last valid position
    	map_lower48.panTo(lastValidCenter_lower48);
	});

	map_alaska = new google.maps.Map(document.getElementById('map_canvas_2'), {
		center: ALASKA,
		zoom: ALASKAzoom,
		minZoom: ALASKAzoom,
		maxZoom: 7,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: true,
		zoomControlOptions: {
       		position: google.maps.ControlPosition.LEFT_BOTTOM
   		},
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false,
		styles: stylesArray,	
	});
	var lastValidCenter_alaska = map_alaska.getCenter();
	google.maps.event.addListener(map_alaska, 'center_changed', function() {
    	if (allowedBounds_alaska.contains(map_alaska.getCenter())) {
       	 	// still within valid bounds, so save the last valid position
    	    lastValidCenter_alaska = map_alaska.getCenter();
    	    return; 
    	}
    	// not valid anymore => return to last valid position
    	map_alaska.panTo(lastValidCenter_alaska);
	});
	
	/* temp removed
	map_germany = new google.maps.Map(document.getElementById('map_canvas_3'), {
		center: GERMANY,
		zoom: GERMANYzoom,
		minZoom: GERMANYzoom,
		maxZoom: 8,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: true,
		zoomControlOptions: {
       		position: google.maps.ControlPosition.LEFT_BOTTOM
   		},
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false,
		styles: stylesArray,	
	});
	var lastValidCenter_germany = map_germany.getCenter();
	google.maps.event.addListener(map_germany, 'center_changed', function() {
    	if (allowedBounds_germany.contains(map_germany.getCenter())) {
       	 	// still within valid bounds, so save the last valid position
    	    lastValidCenter_germany = map_germany.getCenter();
    	    return; 
    	}
    	// not valid anymore => return to last valid position
    	map_germany.panTo(lastValidCenter_germany);
	});
	*/
	
	placeMarkers();
}

function placeMarkers() {

	var myLatLng;
	var marker;
	
	var customMarker = {
    	url: '/wp-content/uploads/2016/03/MapPinSmall.png',
    	size: new google.maps.Size(24, 36),
    	origin: new google.maps.Point(0, 0),
    	anchor: new google.maps.Point(12, 36)
    };

<?php
// call all WP District Map data for marker points, by map
$args = array(
	'post_type' => 'district', 
	'posts_per_page' => 5000, 
	'post_status' => 'publish',
	'nopaging' => true,
	'meta_query' => array(
		array(
			'key' => '_wpgmp_metabox_latitude',
			'value' => '',
			'compare' => '!='
		),
		array(
			'key' => '_wpgmp_metabox_longitude',
			'value' => '',
			'compare' => '!='
		),
	),
);

$loop = new WP_Query( $args );
?>

// NUMBER OF MARKERS: <?= $loop->found_posts ?>

<?php
while ( $loop->have_posts() ) : $loop->the_post();
	// _wpgmp_location_address, _wpgmp_metabox_latitude, _wpgmp_metabox_longitude, _wpgmp_map_id
	?>
	
		myLatLng = {lat: <?= get_post_meta( get_the_ID(), '_wpgmp_metabox_latitude', true); ?>, lng: <?= get_post_meta( get_the_ID(), '_wpgmp_metabox_longitude', true); ?>};
		marker = new google.maps.Marker({
			position: myLatLng,
			map: map_lower48,
			icon: customMarker,
			title: '<?= get_the_title(); ?>',
			url: '<?= get_permalink(); ?>',
			id: 'lower48_<?= get_the_ID(); ?>'
		});
    	markers.push( marker );
		google.maps.event.addListener(marker, 'click', function() {
       		//window.location.href = this.url;
       		populateResultsPanel(<?= get_the_ID(); ?>);
    	});
		
		// put all markers on Germany map 
		/* temp removed
		marker = new google.maps.Marker({
			position: myLatLng,
			map: map_germany,
			icon: customMarker,
			title: '<?= get_the_title(); ?>',
			url: '<?= get_permalink(); ?>',
			id: 'germany_<?= get_the_ID(); ?>'
		});
    	markers.push( marker );
		google.maps.event.addListener(marker, 'click', function() {
       		//window.location.href = this.url;
       		populateResultsPanel(<?= get_the_ID(); ?>);
    	});
    	*/
		
		// put all markers on Alaska map
		marker = new google.maps.Marker({
			position: myLatLng,
			map: map_alaska,
			icon: customMarker,
			title: '<?= get_the_title(); ?>',
			url: '<?= get_permalink(); ?>',
			id: 'alaska_<?= get_the_ID(); ?>'
		});
    	markers.push( marker );
		google.maps.event.addListener(marker, 'click', function() {
       		//window.location.href = this.url;
       		populateResultsPanel(<?= get_the_ID(); ?>);
    	});
		
		<?php 
		
		// get District Works categories for this item
		$args = array(
			'fields' 		   => 'names',
		);
		$thisCategoriesArray = wp_get_post_categories( get_the_ID(), $args );
		
		?>
		
		// put this district into object
		thisDistrictArray = [
			"<?= get_the_ID(); ?>",
			"<?= get_the_title(); ?>",
			"<?= preg_replace("/\r\n|\r|\n/",'<br/>',get_post_meta( get_the_ID(), 'district_address', true)); ?>",
			"<?= get_post_meta( get_the_ID(), 'district_state', true); ?>",
			"<?= get_post_meta( get_the_ID(), 'district_locale', true); ?>",
			"<?= get_post_meta( get_the_ID(), 'superintendent_name', true); ?>",
			"<?= get_post_meta( get_the_ID(), 'percent_free_and_reduced_lunch', true); ?>",
			"<?= get_post_meta( get_the_ID(), 'percent_students_with_access_to_high_speed_broadband', true); ?>",
			"<?= get_post_meta( get_the_ID(), 'percent_students_k-12_with_access_to_personal_school-provided_device', true); ?>",
			"<?= get_post_meta( get_the_ID(), 'number_of_students', true); ?>",
			[
				<?php
				foreach ($thisCategoriesArray as $v) {
					echo '"' . $v . '",';
				}		
				?>
			],
			"<?= get_post_meta( get_the_ID(), '_wpgmp_metabox_latitude', true); ?>",
			"<?= get_post_meta( get_the_ID(), '_wpgmp_metabox_longitude', true); ?>",
			"<?= get_permalink( get_the_ID() ); ?>"
		];
		
		// push into all districts array
		//console.log(thisDistrictArray.toString());
		districtsArray.push(thisDistrictArray);
		
		
	<?php
endwhile; // end of the loop. 

wp_reset_postdata(); 

?>
	
	printFiltersCategories();
	
}

function printFiltersCategories() {
	// DISTRICT WORKS LIST
	thisHTML = "";
	for (var i = 0; i < districtWorksListArray.length; i++) {
    	thisHTML += "<input type='checkbox' name='district_works' value='" + districtWorksListArray[i] + "' onclick='filterMarkers()'>" + districtWorksListArray[i] + "<br/>";
    }
	document.getElementById('district_works_list').innerHTML = thisHTML;
	
	// DP INITIATIVES LIST
	thisHTML = "";
	for (var i = 0; i < dpInitiativesListArray.length; i++) {
    	thisHTML += "<input type='checkbox' name='dp_initiatives' value='" + dpInitiativesListArray[i] + "' onclick='filterMarkers()'>" + dpInitiativesListArray[i] + "<br/>";
    }
	document.getElementById('district_dp_initiatives_list').innerHTML = thisHTML;

}

function resetAllMarkers (resetMaps) {
	for (var i = 0; i < districtsArray.length; i++) {
		toggleMarker(districtsArray[i][0],true);
	}
	var inputs = document.getElementsByTagName('input');
	for (var i=0; i<inputs.length; i++){
		if(inputs[i].getAttribute('type')=='checkbox'){
			inputs[i].checked = false;
		}
	}
	document.getElementById('mapstatefilterselect').selectedIndex = "0";
	
	jQuery( "#maptopbuttons_bystate" ).toggleClass( "btn-gold", false );
	jQuery( "#maptopbuttons_bystate" ).toggleClass( "btn-blue-light", true );

	jQuery( "#maptopbuttons_filter" ).toggleClass( "btn-gold", false );
	jQuery( "#maptopbuttons_filter" ).toggleClass( "btn-blue-light", true );

	if (resetMaps) {	
		map_lower48.setCenter(USA);
		map_lower48.setZoom(USAzoom);
		map_alaska.setCenter(ALASKA);
		map_alaska.setZoom(ALASKAzoom);
		//map_germany.setCenter(GERMANY);
		//map_germany.setZoom(GERMANYzoom);
	}
	
	return false;
}

function filterMarkers () {
	numberOfCheckedBoxes = 0;
	for (var i = 0; i < districtsArray.length; i++) {
		// toggle all OFF to start
		toggleMarker(districtsArray[i][0],false);
		
		//console.log("marker: " + districtsArray[i][0] + " locale: " + districtsArray[i][4]);
		
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
// MULTIDIMENSIONAL ARRAY such as 'district_locale',4,'single'
// so it will look for elements by that name, and compare values in the array slot
// and determine if it's a single, range, or array
		// district_locale, 4, single
		// percent_free_reduced_lunch, 6, range
		// percent_broadband, 7, range
		// percent_1_to_1, 8, range
		// district_works, 10, array
		// dp_initiatives, 10, array
		
		var checkboxes = [
  			['district_locale',4,'single'],
  			['percent_free_reduced_lunch',6,'range'],
  			['percent_broadband',7,'range'],
  			['percent_1_to_1',8,'range'],
  			['district_works',10,'array'],
  			['dp_initiatives',10,'array']
  		];
  		
  		for (var j = 0; j < checkboxes.length; ++j) {
			boxes = document.getElementsByName(checkboxes[j][0]);
			for (var k = 0; k < boxes.length; ++k) {
    			if (boxes[k].checked) {
    				numberOfCheckedBoxes ++;
    				
    				if (checkboxes[j][2] === "array") {
    					// array
						for (var l = 0; l < districtsArray[i][checkboxes[j][1]].length; ++l) {
							if (districtsArray[i][checkboxes[j][1]][l] === boxes[k].value) {
								toggleMarker(districtsArray[i][0],true);
							}
						}
					} else if (checkboxes[j][2] === "range") {
						// range
						var rangeArray = boxes[k].value.split("-");
						//alert(rangeArray[0] + " - " + rangeArray[1]);
						var thisNum = Number(checkboxes[j][1]);
						if (Number(districtsArray[i][thisNum]) >= Number(rangeArray[0]) && Number(districtsArray[i][thisNum]) <= Number(rangeArray[1])) {
							toggleMarker(districtsArray[i][0],true);
						}
					} else {
						// single
    					if (districtsArray[i][checkboxes[j][1]] === boxes[k].value) {
							toggleMarker(districtsArray[i][0],true);
						}
					}
    			}
    		}
		}
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

	}
	if (numberOfCheckedBoxes === 0) {
		jQuery( "#maptopbuttons_filter" ).toggleClass( "btn-gold", false );
		jQuery( "#maptopbuttons_filter" ).toggleClass( "btn-blue-light", true );
		resetAllMarkers(false);
	} else {
		jQuery( "#maptopbuttons_filter" ).toggleClass( "btn-blue-light", false );
		jQuery( "#maptopbuttons_filter" ).toggleClass( "btn-gold", true );
	}
}

function filterStates () {
	thisState = document.getElementById('mapstatefilterselect');
	thisSelectedState = thisState.options[thisState.selectedIndex].value;
	for (var i = 0; i < districtsArray.length; i++) {
		// toggle all OFF to start
		toggleMarker(districtsArray[i][0],false);
	
    	if (districtsArray[i][3] === thisSelectedState) {
			toggleMarker(districtsArray[i][0],true);
		}
	}
	if (thisSelectedState === "All Locations") {
		jQuery( "#maptopbuttons_bystate" ).toggleClass( "btn-gold", false );
		jQuery( "#maptopbuttons_bystate" ).toggleClass( "btn-blue-light", true );
		resetAllMarkers(false);
	} else {
		jQuery( "#maptopbuttons_bystate" ).toggleClass( "btn-blue-light", false );
		jQuery( "#maptopbuttons_bystate" ).toggleClass( "btn-gold", true );
	}
}

var toggleMarker = function (id, showbool) {
	for (var i = 0; i < markers.length; i++) {
		if ((markers[i].id === 'lower48_' + id) || (markers[i].id === 'germany_' + id) || (markers[i].id === 'alaska_' + id)) {
			//console.log ("toggle " + id + " to " + showbool);
	    	markers[i].setVisible(showbool);
	    }
    }
}

function toggleFilters() {
	jQuery("#mapstatefilter").slideUp(250);
	if (document.getElementById('mapactivefilters').style.display === 'none') {
		//document.getElementById('mapactivefilters').style.display = 'block';
		jQuery("#mapactivefilters").slideDown(1000);
	} else {
		//document.getElementById('mapactivefilters').style.display = 'none';
		jQuery("#mapactivefilters").slideUp(1000);
	}
}

function toggleStates() {
	jQuery("#mapactivefilters").slideUp(1000);
	if (document.getElementById('mapstatefilter').style.display === 'none') {
		//document.getElementById('mapstatefilter').style.display = 'block';
		jQuery("#mapstatefilter").slideDown(250);
	} else {
		//document.getElementById('mapstatefilter').style.display = 'none';
		jQuery("#mapstatefilter").slideUp(250);
	}
}

function clearResultsPanel() {
	thisHTML = "Select a district by clicking any pin on the map.";
	document.getElementById('panel-content').innerHTML = thisHTML;
}

function populateResultsPanel(id) {
	thisHTML = "Select a district by clicking any pin on the map.";
	for (var i = 0; i < districtsArray.length; i++) {
		if (districtsArray[i][0] == id) {
			thisHTML = "";
			thisHTML += "<h2 style='text-transform:uppercase;'>" + districtsArray[i][1] + "</h2>";
			thisHTML += "<p>" + districtsArray[i][2] + "</p>";
			thisHTML += "<p>Superintendent, " + districtsArray[i][5] + "</p>";
			thisHTML += "<p><strong>Initiatives:</strong> ";
				thisInitList = districtsArray[i][10].join(", ");
				if (thisInitList.length > 60) {
					n = thisInitList.indexOf(",", 40);
					thisHTML += thisInitList.substring(0,n) + "...";
				} else {
					thisHTML += thisInitList;
				}
			thisHTML += "</p>";
			thisHTML += "<div style='overflow:hidden;'>";
			thisHTML += "<div class='wrapper' style='float:left; width:46%; margin:0 2% 10px; min-height:100px;'>";
			thisHTML += "   <div class='district-data-display'><p>" + districtsArray[i][9] + "</p></div>";
			thisHTML += "   <div class='district-data-label'><p>Number of Students</p></div>";
			thisHTML += "</div>";
			thisHTML += "<div class='wrapper' style='float:left; width:46%; margin:0 2% 10px; min-height:100px;'>";
			thisHTML += "   <div class='district-data-display'><p>" + districtsArray[i][6] + "%</p></div>";
			thisHTML += "   <div class='district-data-label'><p>Free and Reduced Lunch</p></div>";
			thisHTML += "</div>";
			thisHTML += "<div class='wrapper' style='float:left; width:46%; margin:0 2% 10px; min-height:140px;'>";
			thisHTML += "   <div class='district-data-display'><p>" + districtsArray[i][7] + "%</p></div>";
			thisHTML += "   <div class='district-data-label'><p>Students with Access to High Speed Broadband</p></div>";
			thisHTML += "</div>";
			thisHTML += "<div class='wrapper' style='float:left; width:46%; margin:0 2% 10px; min-height:140px;'>";
			thisHTML += "   <div class='district-data-display'><p>" + districtsArray[i][8] + "%</p></div>";
			thisHTML += "   <div class='district-data-label'><p>K-12 Students with Access to a Personal School-provided Device (1:1)</p></div>";
			thisHTML += "</div>";
			thisHTML += "</div>";
			thisHTML += "<p style='text-align:right; margin-right:15px;'><a href='" + districtsArray[i][13] + "' style='float:none;' class='btn btn-blue-light btn-action'>More info</a></p>";

			break;
		}
	}

	document.getElementById('panel-content').innerHTML = thisHTML;
	
	if (1 === 1) { // need to check to ensure panel is not already open, so we don't accidentally close it
		document.getElementById('panel-toggle-tab').click();
	}
}

function orderMapList() {
	//alert("order list!");
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $thisGMAPIkey; ?>&callback=initMap" async defer></script>
                            
<!-- ******************* E N D   M A P ********************* -->
<!-- ******************************************************* -->

                            <form action="">
                            
                            <div class="mapbottombuttons"><p>Order: <input type="radio" name="maplist_order" value="state" onclick="orderMapList()">By State | <input type="radio" name="maplist_order" value="alpha" checked onclick="orderMapList()">Alphabetical</p></div>
                            
                            </form>
                            
                            <div class="maplist" style="-webkit-column-count: 3; -moz-column-count: 3; column-count: 3;">
                            
                            	<div style="display: inline-block;">
                            	<h2 style='text-transform:uppercase;'>Sample State 1</h2>
                            	<h3 style="margin-top:0;">Sample District A</h3>
                            	<p class='superintendent-label'>Superintendent, Alan Superfly</p>
                            	<h3 style="margin-top:0;">Sample District B</h3>
                            	<p class='superintendent-label'>Superintendent, Bob Superfly</p>
                            	<h3 style="margin-top:0;">Sample District C</h3>
                            	<p class='superintendent-label'>Superintendent, Cindy Superfly</p>
								</div>

                            	<div style="display: inline-block;">
                            	<h2 style='text-transform:uppercase;'>Sample State 2</h2>
                            	<h3 style="margin-top:0;">Sample District A</h3>
                            	<p class='superintendent-label'>Superintendent, Alan Superfly</p>
                            	<h3 style="margin-top:0;">Sample District B</h3>
                            	<p class='superintendent-label'>Superintendent, Bob Superfly</p>
                            	<h3 style="margin-top:0;">Sample District C</h3>
                            	<p class='superintendent-label'>Superintendent, Cindy Superfly</p>
								</div>
                            	
                            </div>
                            
                        </div>
                    

                     <?php  endif; ?>

                <?php endwhile; ?>
                <?php posts_nav_link(); ?>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>

   </div>


</div>
<!-- end content container -->


<?php
//print_r( $thisCategoriesArray );
?>

<?php get_footer(); ?>

