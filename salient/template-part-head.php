<?php global $dm_settings; ?>

<div class="outer-wrapper">
    
    
    
    
<div class="container fixed">
    

    
<?php if ($dm_settings['show_header'] != 0) : ?>

    <div class="row dmbs-header">
        <div class="search-bar"><?php dynamic_sidebar( 'search_bar_1' ); ?></div>
        
        
        <div style="position:relative;">
            <div class="dmbs-header-text">
            
            
            <div class="utility-bar">
                <?php dynamic_sidebar( 'utility_nav_1' ); ?>
                <?php 
                    wp_nav_menu(array(
                        'theme_location' => 'utility_navigation', // menu slug from step 1
                        'container' => false, // 'div' container will not be added
                        'menu_class' => 'nav', // <ul class="nav"> 
                        'fallback_cb' => 'default_utility_nav', // name of default function from step 2
                    ));
                ?>
                </div>
            
            <?php if ( get_header_textcolor() != 'blank' ) : ?>
                <h1><a class="custom-header-text-color" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                <h4 class="custom-header-text-color"><?php bloginfo( 'description' ); ?></h4>
            <?php endif; ?>
            <?php else : ?>
                <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                <h4><?php bloginfo( 'description' ); ?></h4>
            <?php endif; ?>
            
            <div class="main-nav-bar">
            
                <?php if ( has_nav_menu( 'main_menu' ) ) : ?>

                    <nav class="col-xs-7 col-xs-push-5 navbar navbar-inverse" role="navigation">

                            <?php
                            wp_nav_menu( array(
                                    'theme_location'    => 'main_menu',
                                    'depth'             => 2,
                                    'container'         => 'div',
                                    'container_class'   => 'collapse navbar-collapse navbar-1-collapse',
                                    'menu_class'        => 'nav navbar-nav',
                                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                    'walker'            => new wp_bootstrap_navwalker())
                            );
                            ?>
                    </nav>

            <?php endif; ?>
            </div><!-- END MAIN NAV BAR -->                

                
            
            <!-- HEADER LOGO IMAGE -->
            <?php if ( get_header_image() != '' || get_header_textcolor() != 'blank') : ?>

            <?php if ( get_header_image() != '' ) : ?>
                <div class="dmbs-header-img text-center">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></a>
                </div>
            <?php endif; ?>
                
                
            </div><!-- END RELATIVE POSITION DIV -->
            
            
            
            
        </div>

    </div>

<?php endif; ?>
</div><!-- end container fixed -->
<div class="container dmbs-container">
    

  