// JavaScript Document

var corpusData;
var corpusNodes;
var corpus;
var topicSelection = "ALL";
var subtopicSelection = "ALL";
var fieldSelection = "K";

var topicSelect;
var subtopicSelect;
var fieldSelect;


jQuery("#corpusTable .third").removeClass("hid").hide();
jQuery("#corpusTable .fourth").removeClass("hid").hide();
jQuery(function(){
	jQuery("#corpusTable .third").removeClass("hid").hide();
	jQuery("#corpusTable .fourth").removeClass("hid").hide();
	jQuery.getJSON("/wp-content/themes/researchmap/data/clusters_thr2.json",[],topicsGathered);
});

function topicsGathered(data){
	corpusData = data;
	corpusNodes = data.nodes;

	
	corpus = jQuery("#corpusTable");
	
	topicSelect = jQuery("#topicSelect");
	subtopicSelect = jQuery("#subtopicSelect");
	fieldSelect = jQuery("#fieldSelect");
	
	
	topicSelect.on("change",populateSubSelections);
	subtopicSelect.on("change",populateTable);
	fieldSelect.on("change",populateTable);
	
	
	populateSelections();
}

function populateSelections(){
	
	var topOp = jQuery("<option>")
		.attr("value","ALL")
		.prop("selected",true)
		.html("All Topics and Subtopics (" +wholecorpus.size+" entries)");
		
	topicSelect.append(topOp);
	
	corpusNodes.forEach(function(n){
		if (n.type =="top"){
			var op = jQuery("<option>")
				.attr("value",n.name)
				.html("Topic: "+n.label+" ("+n.size+" entries)");
				
			topicSelect.append(op);
		}
	});
	
	populateSubSelections();
	
}

function populateSubSelections(){
	topicSelection = topicSelect.val();

	subtopicSelection = "ALL";
	if (topicSelection == "ALL"){
		subtopicSelect.hide();
		if (fieldSelect.val().length >1){
          fieldSelect.val("K");
        }
        document.getElementById("MCPselect").disabled = true
        document.getElementById("MRPselect").disabled = true
        document.getElementById("MCAselect").disabled = true
	}
	else{
		document.getElementById("MCPselect").disabled = false
        document.getElementById("MRPselect").disabled = false
        document.getElementById("MCAselect").disabled = false
		subtopicSelect.html("");
		
		var topOp = jQuery("<option>")
			.attr("value","ALL")
			.prop("selected",true)
			.html("All subtopics");
			
		subtopicSelect.append(topOp);
		
		var subs = corpusNodes.forEach(function(n){
			if( n.type == "subtop" && n.id_top == topicSelection){
				var op = jQuery("<option>")
				.attr("value",n.name)
				.html("Subtopic: "+n.label+" ("+n.size+" entries)");
				
			subtopicSelect.append(op);
			}
		});
		subtopicSelect.show();
	}
		
	populateTable();
}

function populateTable(){
	
	var infoNode;
	fieldSelection = fieldSelect.val();
	
	if (topicSelection =="ALL")
		infoNode = wholecorpus;
	else{
		subtopicSelection = subtopicSelect.val();
		infoNode = corpusNodes.filter(function(d){
			if (subtopicSelection != "ALL")
				return (d.name == subtopicSelection)
			else
				return (d.name == topicSelection)
			})[0]
	}
	
	
	
	
	
	/*if (myF==='MCP'){thelist=prepList(n.MCP,n.size,'Most Cited Papers',myC)}
      if (myF==='MRP'){thelist=prepList(n.MRP,n.size,'Highly Representative Papers',myC)}
      if (myF==='MCA'){thelist=prepList(n.MCAU,n.size,'Most Cited Authors',myC)}
      //if (myF==='A'){thelist=prepList(n.stuffA,n.size,'Authors',myC)}
      if (myF==='J'){thelist=prepList(n.stuffJ,n.size,'Most Frequent Journals',myC)}
      if (myF==='K'){thelist=prepList(n.stuffK,n.size,'Most Frequent Keywords',myC)}
      if (myF==='T'){thelist=prepList(n.stuffTK,n.size,'Most Frequent Title words',myC)}
      if (myF==='I'){thelist=prepList(n.stuffI,n.size,'Most Frequent Institutions',myC)}
      if (myF==='Y'){thelist=prepList(n.stuffY,n.size,'Most Frequent Years',myC)}
      if (myF==='R'){thelist=prepList(n.stuffR,n.size,'Most Frequent References',myC)}
      //if (myF==='RJ'){thelist=prepList(n.stuffRJ,n.size,'Most Frequent Reference Journals',myC)} 
      if (myF==='S'){thelist=prepList(n.stuffS,n.size,'Most Frequent Subject Categories',myC)*/
	  
	  buildList(infoNode[decode[fieldSelection][0]],infoNode.size,decode[fieldSelection][1]);
	
}

function buildList(info, size, title){
	
	jQuery(".nonHeader",corpus).remove();
	
	
	
	jQuery("#fieldHeader").html(title.toUpperCase());
	jQuery("#corpusTable .third").hide();
	jQuery("#corpusTable .fourth").hide();
	
	if (fieldSelection == "MCA"){
		jQuery("th.third").html("Publications".toUpperCase());
		jQuery("th.fourth").html("Citations".toUpperCase());
		jQuery("#corpusTable .third").show();
		jQuery("#corpusTable .fourth").show();
	}
	else if (fieldSelection == "MRP" || fieldSelection == "MCP"){
		jQuery("#corpusTable .fourth").hide();
		jQuery("th.third").html("Citations".toUpperCase());
		jQuery("#corpusTable .third").show();
	}
	else{
		jQuery("th.third").html("COUNT".toUpperCase());
		jQuery("th.fourth").html("%".toUpperCase());
		setTimeout(function(){
			jQuery("#corpusTable .third").hide();
	jQuery("#corpusTable .fourth").hide();
		},200)
		
	}
	
	var ranking = 0;
	console.log("populating the table with data");
	
	info.forEach(function(n,ind){
		
		if (topicSelection == "ALL" && ind >= 50 && fieldSelection != "J"){
			console.log ("cut off point");
			return;
		}
		
		var row = jQuery("<tr>");
		row.attr("class","nonHeader");
		
		row.append(jQuery("<td>").html(numberWithCommas(++ranking)).addClass("first"));
		if (fieldSelection == "MRP" || fieldSelection == "MCP"){
			var nCol = jQuery("<td>").addClass("second").html(n[1]+", <a href='" + wwwlink(n[0]) + "' target='new'>"+ n[0]+'</a>, <i>'+n[2]+' ('+n[3]+")</i>");
			var lCol = jQuery("<td>").addClass("third").html(numberWithCommas(n[4]));
			
			row.append(nCol);
			row.append(lCol);
		} 
		else if (fieldSelection == "MCA"){
			var nCol = jQuery("<td>").addClass("second").html("<a href='" + wwwlink(n[0]) + "' target='new'>"+ n[0]+"</a>");
			var slCol = jQuery("<td>").addClass("third").html(numberWithCommas(n[2]));
			var lCol = jQuery("<td>").addClass("fourth").html(numberWithCommas(n[1]));
			
			row.append(nCol);
			row.append(slCol);
			row.append(lCol);
		}
		else{
			var nCol = jQuery("<td>").addClass("second").html(n[0]);
			//var lCol = jQuery("<td>").addClass("fourth").html(Math.round(n[1]/size*100));
			//var slCol = jQuery("<td>").addClass("third").html(numberWithCommas(n[1]));
			
			row.append(nCol);
			//row.append(slCol);
			//row.append(lCol);
		}
		
		corpus.append(row);
		
		
	});
	
}

function wwwlink(foo) {
    foo="https://scholar.google.com/scholar?hl=com&q="+foo.replace(' ','+');
    return foo
  }

  function wwwreflink(foo) {
    bar=foo.split(', ')
    foo="https://scholar.google.com/scholar?as_q=&as_epq=&as_oq=&as_eq=&as_occt=any&as_sauthors="+bar[0].replace(' ','+')+"&as_publication=&as_ylo="+bar[1]+"&as_yhi="+bar[1]+"&btnG=&as_sdt=0%2C5"
    return foo
  }

var decode = {
	"MCP":["MCP","Paper"],
	"MRP":["MRP","Paper"],
	"MCA":["MCAU","Author"],
	"J":["stuffJ","Journal"],
	"K":["stuffK","Keyword"],
	"T":["stuffTK","Title Word"],
	"I":["stuffI","Institution"],
	"Y":["stuffY","Year"],
	"R":["stuffR","Reference"],
	"S":["stuffS","Subject Category"]
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}