var rBar;
var lBar;

function positionRightBar(){
		rBar.css({left:jQuery('body').innerWidth() - ((rBar.hasClass("open"))?(rBar.outerWidth()):0)});
		
		
	}
function positionLeftBar(){
	setTimeout(function(){jQuery(".rightTab").css({left:lBar.innerWidth()});},50);
}

function slideOutInfo(){
	rBar.addClass("open");
	rBar.animate({left:jQuery('body').innerWidth() - ((rBar.hasClass("open"))?(rBar.innerWidth()):0)},300);
	var slide = jQuery("#rightSlideTab");
	slide.removeClass("goingLeft");
	slide.addClass("goingRight");
}

function slideInInfo(){
}
	


jQuery(function(){
	
	rBar = jQuery(".rightSidebar");
	lBar = jQuery(".leftSidebar");

	
	
	jQuery(window).resize(function(){
		positionRightBar();
		positionLeftBar();
	});
	
	var slide = jQuery("#rightSlideTab");
	slide.click(function(){
		rBar.toggleClass("open");
		if (rBar.hasClass("open")){
			slide.removeClass("goingLeft");
			slide.addClass("goingRight");
		}else{
			slide.addClass("goingLeft");
			slide.removeClass("goingRight");
		}
		rBar.animate({left:jQuery('body').innerWidth() - ((rBar.hasClass("open"))?(rBar.outerWidth()):0)},300);
	});
	
	var lSlide = jQuery("#leftSlideTab");
	lSlide.click(function(){
		lBar.animate({left:-1*lBar.outerWidth()},300);
		jQuery(".rightTab").addClass("backTab");
		lSlide.removeClass("backTab");
		lSlide.addClass("goingRight");
		lSlide.removeClass("goingLeft");
	});
	
	var key = jQuery("#keyTab");
	key.click(function(){
		lBar.animate({left:0},300);
		jQuery("#keyPanel").removeClass("inactive");
		jQuery("#searchPanel").addClass("inactive");
		jQuery(".rightTab").addClass("backTab");
		key.removeClass("backTab");
		lSlide.removeClass("goingRight");
		lSlide.addClass("goingLeft");
		
	});
	
	var filt = jQuery("#filterTab");
	filt.click(function(){
		lBar.animate({left:0},300);
		jQuery("#keyPanel").addClass("inactive");
		jQuery("#searchPanel").removeClass("inactive");
		jQuery(".rightTab").addClass("backTab");
		filt.removeClass("backTab");
		lSlide.removeClass("goingRight");
		lSlide.addClass("goingLeft");
		
	});
	
	positionRightBar();
	positionLeftBar();
	
	setTimeout(function(){
		if (graph_type == "FORCE"){
			jQuery(".mapName").html("Force-Directed Map");
		}
		else {
			jQuery(".mapName").html("Chord Map");
		}
	},100);
});