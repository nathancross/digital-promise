<?php /* Template Name: Our Team Landing Page */ ?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


<div class="row dmbs-content" style="margin-bottom:20px;">

    <div class="col-md-3">
    	<h2 class="page-header" style="margin-bottom:0;">Our Team</h2>
    </div>
    
    <div class="col-md-9">
        
        <form style="float:right; margin-top:4.25rem;" id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ) . 'our-team/'; ?>" method="get">
            
            <?php
            $selected = '';
            $term = false;
            
            if(isset($_GET['team-members']))
            {
                if(term_exists($_GET['team-members'], 'group'))
                {
                    $selected = $_GET['team-members'];
                    $term = true;
                }
            }
            
            $args = array(
                'show_option_all'   => 'All',
                'show_count'        => 0,
                'orderby'           => 'name',
                'echo'              => 0,
                'selected'          => $selected,
                'value_field'       => 'slug',
                'name'              => 'team-members',
                'taxonomy'          => 'group',
            );
            
            $select  = wp_dropdown_categories( $args );
            $replace = "<label for='cat'>Filter by:</label> <select$1 onchange='return this.form.submit()'>";
            $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); 
            
            ?>

            <?php echo $select; ?>

            <noscript>
                <input type="submit" value="View" />
            </noscript>

        </form>


    </div>
</div>


<!--------------------------------- TESTING AREA --------------------------------->
<?php /*
<div style="background:yellow; padding:20px; margin:20px; border:2px solid black;">
    <h2>Sandbox testing area... hardhats required.</h2>
    <p>Order alphabetically by last_name field...</p>
    <?php 

    // query
    $the_query = new WP_Query(array(
        'post_type'			=> 'our-team',
        'posts_per_page'	=> -1,
        'meta_key'			=> 'last_name',
        'orderby' => 'meta_value',
        'order'	=> 'ASC'
    ));

    ?>
    <?php if( $the_query->have_posts() ): ?>
        <ul>
        <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <li>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
        <?php endwhile; ?>
        </ul>
    <?php endif; ?>

    <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
</div>
*/ ?>
<!------------------------------------- END ------------------------------------->


<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">
        
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
        <?php 
        if($term)
        {
            $args = array( 
                'post_type' => 'our-team', 
                'posts_per_page' => 500,
                'meta_key' => 'last_name',
                'orderby' => 'date meta_value',
                'order' => 'ASC',
                'tax_query' => array(
                	array(
                    	'taxonomy' => 'group',
                        'field' => 'slug',
                        'terms' => $selected,
                    ),
                ),
            );
        } else {
            $args = array( 
            	'post_type' => 'our-team', 
            	'posts_per_page' => 500,
                'meta_key' => 'last_name',
                'orderby' => 'meta_value',
                'order' => 'ASC',
            );
        }
        $loop = new WP_Query( $args );
        $inc = 0;

        while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php $inc++; ?>
        <div class="Grid-cell u-large-1of5 u-med-1of4 u-small-1of2">

                <div class="team-member">
                    <div <?php post_class(); echo 'id="bio'.$inc.'"'; ?>>

                        <?php get_template_part( 'content', 'page' ); ?>

                            <?php if ( has_post_thumbnail() ) : ?>
                                               <?php the_post_thumbnail("medium"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>

                            <h3>
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h3>
                        <!-- print position title... -->
                        <?php echo get_post_meta( get_the_ID(), 'position_title', true); ?>
                        <div class="bio-toggle" style="display:none;">
                            <a class="toggle" <?php post_class(); echo 'id="bio'.$inc.'"'; ?> >More</a>
                        </div>
                        <div class="bio-container">
                            <div style="max-width:1170px; margin:0 auto;">

                                <div style="max-width:1170px; margin:0 auto;">
                                    <div class="row">
                                        <div class="col-md-6 bio-title">
                                            <h3><?php the_title(); ?></h3>
                                            <p class="bio-position-title"><?php echo get_post_meta( get_the_ID(), 'position_title', true); ?></p>
                                        </div>
                                        <div class="col-md-6 bio-content">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                    <p style="text-align:center; margin-top:30px;"><a style="float:none; color:black;" class="toggle bio-close btn btn-gold" style="color:white;">Close</a></p>

                                </div>

                                
                                
                            </div>
                            
                        </div>
                        
                    </div>
                        
                </div>
            
                

            </div>
            

		<?php endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div><!-- /end grid container -->

            

   </div>


</div>
<!-- end content container -->

<div class="bio-hidden-panel">
</div>

<?php get_footer(); ?>

