<html>
    <head>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/digital-promise/style.css" />
    </head>
    <body class="tool-redirect" style="background:#439AD1; text-align:center; color:white;">




            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>
                            
                            <img src="/wp-content/themes/digital-promise/img/Digital-Promise-Logo-White-small.png">
                            
                            <h2 class="page-header">You will be redirected to "<?php the_title() ;?>" in 5 seconds...</h2>
                            
                            <!-- print link... -->
                            <?php //the_meta(); ?>
                            
                                    <script type="text/javascript">   
                                        function Redirect() 
                                        {  
                                        	window.location= "<?php echo get_post_meta ( get_the_ID(), 'tool_url', true );?>"; 
                                        } 
                                        setTimeout('Redirect()', 1);   
                                    </script>
                                    
                            <p><a href="<?php echo get_post_meta ( get_the_ID(), 'tool_url', true );?>">Click here</a> if you are not automatically redirected.</p>
                            
                            <p>&nbsp;</p>
                            
                            <a style="margin:0 auto; float:none;" class="btn btn-gold" href="javascript: window.history.go(-1)">Go back</a>
                        </div>
                    

                     <?php  endif; ?>

                <?php endwhile; ?>

                <?php else: ?>

            <?php endif; ?>


    </body></html>