<div id="sidebarFilter" class="slide sidebar large-2 medium-3 leftSidebar slideOut" role="complementary">
	 <div id="leftSlideTab" class="rightTab tab backTab goingLeft"></div>
    <div id="keyTab" class="rightTab tab backTab"><div  class="vertical-text tab-text">key</div></div>
    <div id="filterTab" class="rightTab tab"><div  class="vertical-text tab-text">filter</div></div>

    <div id="searchPanel">
        <div class="panelTitle">
          <span class="mapName">Force-Directed Map</span><br>
          2005-2014 Data
        </div> 
        <br>
        <div id="level"></div>
        <div id="search"></div>
        <div id="filters"></div>
    </div>
   
    <div id="keyPanel" class="inactive">
    <div class="panelTitle">
      <span class="mapName">Network Map</span><br>
      Key
    </div> 
  	<br>
    <span class="forceKey">
      <div class="line">
      
        <svg height="14" width="15"><circle cx="8" cy="7" r="7" fill="lightgreen" /></svg>
        <b>Circles</b> represent research topics. Circle <i>size</i> is proportional to the number of publications on that topic.
      </div>
	<br>
      <div class="line">
        <svg height="15" width="15"><line x1="3" y1="15" x2="14" y2="1" style="stroke: #B2D9D8;stroke-width:3" /></svg>
        
        <b>Lines</b> represent connections between topics. Line <i>thickness</i> is proportional to the similarity of two topics.
      </div>  
	<br>
      <div class="line">
        <svg height="15" width="15">
          <polygon points="8,8,8,16,15,12" style="fill:magenta;" />
          <polygon points="8,8,15,12,15,4" style="fill:royalblue;" />
          <polygon points="8,8,15,4,8,0" style="fill:lightgreen;" />
          <polygon points="8,8,8,0,1,4" style="fill:yellow;" />
          <polygon points="8,8,1,4,1,12" style="fill:orange;" />
          <polygon points="8,8,1,12,8,16" style="fill:red;" />
        </svg> 
        <b>Colors</b> represent research topics.
      </div>
      <br>
     </span>
      <span class="chordKey">
      <div class="line">
      
        <!--<svg height="14" width="15"><circle cx="8" cy="7" r="7" fill="lightgreen" /></svg> -->
        <b>Arcs</b> represent research topics. Arc <i>size</i> is proportional to the number of outside references to that topic.
      </div>
	<br>
      <div class="line">
       <!-- <svg height="15" width="15"><line x1="3" y1="15" x2="14" y2="1" style="stroke: #B2D9D8;stroke-width:3" /></svg> -->
        
        <b>Chords</b> represent connections between topics. Chord <i>thickness</i> is proportional to the similarity of two topics.
      </div>  
	<br>
      <div class="line">
        <svg height="15" width="15">
          <polygon points="8,8,8,16,15,12" style="fill:magenta;" />
          <polygon points="8,8,15,12,15,4" style="fill:royalblue;" />
          <polygon points="8,8,15,4,8,0" style="fill:lightgreen;" />
          <polygon points="8,8,8,0,1,4" style="fill:yellow;" />
          <polygon points="8,8,1,4,1,12" style="fill:orange;" />
          <polygon points="8,8,1,12,8,16" style="fill:red;" />
        </svg> 
        <b>Colors</b> represent research topics.
      </div>
      <br>
     </span>
     <div id="colorKey">
     </div>
	
    </div>
   
    
    
    </div>