<?php
/*
Template Name: Blog landing page
*/
get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <div class="col-md-12 dmbs-main">
    
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    	<?php if(function_exists('bcn_display'))
    	{
        	bcn_display();
    	}?>
	</div>

        <h2 class='page-header'>Our Blog</h2>
        
        <?php
        
        //echo $_SERVER["REQUEST_URI"];
        
        if ($_SERVER["REQUEST_URI"] === "/our-blog/") {
        
        ?>
        
        <h2>Featured Stories</h2>

		<div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
        <?php 
        
		$max_to_show = 3;
		$number_shown = 0;        
        
        $args = array(
        	'post_type' => 'post', 
        	'posts_per_page' => 2, 
        	'orderby' => 'date', 
        	'order' => 'DESC',
        	'post_status' => 'publish',
        	'nopaging' => true,
   	        'meta_query' => array(
   	         	array(
   	         		'key' => 'is_featured',
   	         		'value' => true,
   	         		'compare' => '='
   	         		)
   	         ),
        );
        
        
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); 
        
        	$number_shown ++;
        	if ($number_shown <= $max_to_show) {
        ?>
                       
            <div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                           
                           <?php if ( has_post_thumbnail() ) : ?>
                               <?php the_post_thumbnail("medium"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>

                            <h3>
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h3>

                            
                            <?php the_excerpt(); ?>

                       </div>
                    </div>

            </div>

		<?php 
			
			}
			
		endwhile; // end of the loop. ?>

        <?php wp_reset_postdata(); ?>
        </div><!-- /end grid container -->

        <?php
        
       	}
        
        ?>


    </div>
</div>

<div class="row dmbs-content">

    <div class="col-md-6">
        <h2>Recent Posts</h2>
    </div>
    
    <div class="col-md-6">
   
		<!-- determine the current sector to display -->
		<?php  $this_cat = get_query_var( 'cat', 0 );  ?>

        <form style="float:right;" id="category-select" class="category-select" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="get">

            <?php
            $which_selected = 0;
            if ($this_cat > 0) {
            	$which_selected = $this_cat;
            }
            
            $args = array(
                'show_option_all' => __( 'Show all categories' ),
                'orderby'          => 'name',
                'echo'             => 0,
                'show_count'	   => 0,
                'hide_empty'	   => 1,
                'hierarchical'	   => 1,
                'selected'		   => $which_selected,
            );
            ?>

            <?php $select  = wp_dropdown_categories( $args ); ?>
            <?php $replace = "<label for='cat'>Categories:</label> <select$1 onchange='return this.form.submit()'>"; ?>
            <?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

            <?php echo $select; ?>

            <noscript>
                <input type="submit" value="View" />
            </noscript>

        </form>
    </div>
</div>

<div class="row dmbs-content">
    <div class="col-md-12">
        <div class="Grid Grid--gutters Grid--full large-Grid--fit Grid--flexCells">
            <?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post (unused in this template)
                    if ( is_single() ) : ?>
                        
                    <?php
                    // list of posts
                    else : ?>
                    
            <?php
            //echo "is_featured = ";
			$is_featured = get_post_meta($post->ID, 'is_featured', TRUE);
			//if(!$is_featured) {
			?>
			
			<div class="Grid-cell u-large-1of4 u-med-1of2 u-small-full">

                <div class="callout">
                    <div <?php post_class(); ?>>
                           
                           <?php if ( has_post_thumbnail() ) : ?>
                               <?php the_post_thumbnail("medium"); ?>
                                <div class="clear"></div>
                            <?php endif; ?>
							
							<p class="byline vcard" style="margin-bottom:1em;">
								<time class="updated" datetime="<?php echo get_the_date('Y-m-d'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
							</p>

                            <h3>
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h3>

                            
                            <?php the_excerpt(); ?>

                       </div>
                    </div>

            </div>

			<?php
			//}
			?>

				
                     <?php  endif; ?>

                <?php endwhile; ?>
            
        </div><!-- /end grid container -->
                <?php posts_nav_link(); ?>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>
    </div>
   </div>
<!-- end content container -->

<?php get_footer(); ?>

